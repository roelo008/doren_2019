"""
Brief script for visualizing NDep across Europe based on the NDep data from Max Posch.
By: Hans Roelofsen, WEnR, 08-04-2020
"""

import os
import pandas as pd
import geopandas as gp
from shapely import geometry

ndp_compiled_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\POSCH_dep'
nh3_in = 'NH3-20200331.csv'
nox_in = 'NOx-20200331.csv'
nh3 = pd.read_csv(os.path.join(ndp_compiled_dir, nh3_in), sep=',', comment='!', index_col='plot_obs_id')
nox = pd.read_csv(os.path.join(ndp_compiled_dir, nox_in), sep=',', comment='!', index_col='plot_obs_id')
yr_cols = [x for x in list(nh3) if x.isdigit()]
nh3.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)
nox.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)

data_cols = [c.startswith('year') for c in list(nh3)]
totN_mg_m2 = nh3.loc[:, data_cols].add(nox.loc[:, data_cols])
totN_kg_ha = totN_mg_m2.divide(100)
totN_kmol_ha = totN_kg_ha.divide(14)
totN_kmol_ha['lat'] = nh3.Lat
totN_kmol_ha['lon'] = nh3.Lon
totN_kmol_ha['coordinates'] = totN_kmol_ha.apply(lambda row: (row.lon, row.lat), axis=1)
totN_kmol_ha['geometry'] = totN_kmol_ha.coordinates.apply(geometry.Point)
out_gdf = gp.GeoDataFrame(totN_kmol_ha, geometry='geometry')
out_gdf.drop(labels='coordinates', axis=1).to_file(r'c:\apps\temp_geodata\doren\MaxData\MaxNDep.shp')
