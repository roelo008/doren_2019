"""
Select and show plots containing species X on a map. Also map plots residing within the convex hull of all
 selected plots.

Hans Roelofsen, WEnR, team BB, March 2020
"""

import shapely
import os
import datetime
import geopandas as gp
import pandas as pd
from utils import doren as do

'''input parameter'''
sp_name = 'Empetrum nigrum'
eva_species_db = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\4_eva_species\eva_doren_species_selected_200318-1625.csv'
eva_header_db = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\1_EVA\eva_header_selection_20200318-1511.pkl'
base_out_dir = r'c:\apps\proj_code\doren_2019\c_out'

"""read as pandas db"""
s_db = pd.read_csv(eva_species_db, sep=';')  # read species database
h_db = pd.read_pickle(eva_header_db)  # read header database

# set 3035 coordinates as geometry
h_db['plot_3035'] = h_db.plot_coordinates_3035.apply(shapely.geometry.Point)
h_db.set_geometry('plot_3035', inplace=True)

"""header data of plots containing the species"""
plots_w_species = do.query_eva_for_species(s_db, h_db, sp_name)

"""convex hull"""
ply_geom = shapely.geometry.Polygon([[p.x, p.y] for p in plots_w_species.geometry])
cnvx_hull = ply_geom.convex_hull  # shapely polygon, just fine
cnvx_hull_gdf = gp.GeoDataFrame(pd.DataFrame({'ID': 1, 'soort': sp_name, 'geometry': cnvx_hull}, index=[0]))

"""find plots within the convex hull"""
within_hull = h_db.apply(lambda row: cnvx_hull.contains(row.plot_3035), axis=1)
nearby_plots = h_db.loc[within_hull, :]

# write to shapefiles
timestamp_brief = datetime.datetime.now().strftime("%Y%m%d%H%M")
out_dir = os.path.join(base_out_dir, '{0}{1}'.format(timestamp_brief, sp_name))

basename = '{0}_{1}'.format(timestamp_brief, sp_name)
os.mkdir(out_dir)
cnvx_hull_gdf.to_file(os.path.join(out_dir, '{0}_convex_hull.shp'.format(basename)))
plots_w_species.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', 'plot_pnt_wgs84'], axis=1) \
               .to_file(os.path.join(out_dir, '{0}_resident_plots.shp'.format(basename)))
nearby_plots.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', 'plot_pnt_wgs84'], axis=1) \
            .to_file(os.path.join(out_dir, '{0}_nearby_plots.shp'.format(basename)))



