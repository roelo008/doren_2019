"""
Script for quick look assessment of DOREN data

Hans Roelofsen, WEnR team B&B 7 Oct 2019
"""

import os
import pandas as pd
import geopandas as gp
import numpy as np

# Read compiled EVA-EMEP data, containing plot headers
eva_emep_dir = r'd:\DOREN19\b_compiled_data\eva_emep'
eva_gdf_in = 'eva_emep_gdf_190918-1612.pkl'
eva_emep = pd.read_pickle(os.path.join(eva_emep_dir, eva_gdf_in))

# count per country
cntr_count = pd.pivot_table(eva_emep, index='country', aggfunc='count', values='plot_obs_id', dropna=False)
print(cntr_count.sort_values(by='plot_obs_id', ascending=False))
cntr_count.sort_values(by='plot_obs_id', ascending=False).to_clipboard()

# histogram N-deposition
ndep = eva_emep.ndep_tot.describe()
ndep_hist = np.histogram(eva_emep.ndep_tot, bins=[x for x in range(0, 5500, 250)])
ndep_df = pd.DataFrame({'bin': ndep_hist[1][:-1], 'count': ndep_hist[0]}).to_clipboard()