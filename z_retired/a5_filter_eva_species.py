"""
Script to narrow down EVA species data to just the plots remaining from the previous processing steps.

Onderdeel van DOREN project.

Hans Roelofsen, WEnR, team B&B, 19-09-2019

"""

import datetime
import pandas as pd
import os

from utils import doren as do

'''
Read source data
'''
# EVA species per plot
eva_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\EVA'
eva_species_in = r'EVA_Doren_species.csv'
eva_species = pd.read_csv(os.path.join(eva_dir, eva_species_in), sep='\t')  # plot-species source data

# Selected EVA header data
eva_compiled_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\4_EVA_POSCH_Covs_Eobs'
eva_compiled_in = 'eva_headers_w_NDep_MAX_CoVars_Eobs20200421-1824.csv'
eva_compiled = pd.read_csv(os.path.join(eva_compiled_dir, eva_compiled_in), sep=';', comment='#')#, encoding='utf-16')

'''output data'''
out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\5_eva_species'

'''
rename species data columns to sensible format
'''
colnames = list(eva_species)
colnames_n = ['plot_obs_id', 'taxonomy', 'taxon_group', 'taxon_group_id', 'turboveg2_concept', 'matched_concept',
              'match', 'layer', 'cover_perc', 'cover_code']
eva_species.rename(columns=dict(zip(colnames, colnames_n)), inplace=True)

'''
Report on findings
'''
n_plots = len(set(eva_species.plot_obs_id))
n_species = len(set(eva_species.turboveg2_concept))
print('Reading {0} gives {1} unique plots with total {2} different species'.format(eva_species_in, n_plots, n_species))
# sanity check, are there any plots in the compiled data that are NOT in the species-data?
compiled_plots = set(eva_compiled.PlotObservationID)
species_plots = set(eva_species.plot_obs_id)

assert compiled_plots.issubset(species_plots),'There are plots in the compiled header database that are NOT in the ' \
                                              'species database: {0}'.format(compiled_plots.difference(species_plots))

'''
Identify plots from the species database that are also present in the compiled header data
Drop plots that are not part of the compiled data
'''
species_plot_sel = species_plots.intersection(compiled_plots)  # species plots that are WITHIN the compiled header dat
species_plot_drop = species_plots.difference(compiled_plots)  # species plots that are OUTSIDE the compiled header dat

# double check
if len(species_plot_sel) != eva_compiled.shape[0]:
    print('  Note, number of selected species plot data does not equal to number of compiled header data!')
elif len(species_plot_sel) + len(species_plot_drop) != len(species_plots):
    raise Exception('Plots species data must be either in or out the compiled header data!')
else:
    print('Identified the {0} plots from the species data that are also present in the compiled header data '
          '(with {1} rows)'.format(len(species_plot_sel), eva_compiled.shape[0]))

index_to_drop = eva_species.loc[eva_species['plot_obs_id'].isin(list(species_plot_drop)), :].index
eva_species_sel = eva_species.drop(labels=index_to_drop, axis=0)

'''
Dissolve species names to remove subspecies and variants. Then assign unique species numbers
'''
eva_species_sel['species_name_hdr'] = eva_species_sel.turboveg2_concept.apply(do.strip_leading_quote)
eva_species_sel['species_name_hdr'] = eva_species_sel.species_name_hdr.apply(do.simplify_species)

# assign a number to each plant species and add as new column
species_list = [species for species in sorted(list(set(eva_species_sel.species_name_hdr)))]
species_nrs = dict(zip([j for _, j in enumerate(species_list)], [i for i,_ in enumerate(species_list)]))
eva_species_sel['species_nr'] = eva_species_sel['species_name_hdr'].map(species_nrs)
species_nr_list = pd.DataFrame(data={'species_name_hdr': species_list,
                                     "species_nr": [species_nrs[x] for x in species_list]})

# report on species numbers
print('Identified {0} unique species'.format(len(species_list)))

'''
Sort plot inventories first by species and secondly by plot as requested by Wieger
'''
eva_species_ww = eva_species_sel.loc[:, ['plot_obs_id', 'species_nr']].sort_values(by=['species_nr', 'plot_obs_id'])
eva_species_ww.rename(columns={'plot_obs_id': 'nr', 'species_nr': 'soort'}, inplace=True)

'''
Write species list and plot-inventories to file
'''
datestamp = datetime.datetime.now().strftime("%y%m%d-%H%M")

species_nr_list.to_csv(os.path.join(out_dir, 'eva_species_list_{0}.csv'.format(datestamp)), sep=';')

eva_species_sel.to_csv(os.path.join(out_dir, 'eva_doren_species_selected_{0}.csv'.format(datestamp)), sep=';',
                       index=False)

eva_species_ww.to_csv(os.path.join(out_dir, 'eva_doren_species_selected_WWformat_{0}.csv'.format(datestamp)), sep='\t',
                      index=False)
