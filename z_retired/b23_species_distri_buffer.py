"""
Select and show plots containing species X on a map. Also map plots residing within a X km buffer around each selected
plot.

Hans Roelofsen, WEnR, team BB, March 2020
"""

import os
import argparse
import shapely
import datetime
import geopandas as gp
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

'''Parse arguments'''
parser = argparse.ArgumentParser()
parser.add_argument('species', help='species name')
parser.add_argument('buff', help='buffer size in m', type=int)
parser.add_argument('--shp', help='write shapefiles', action="store_true")
parser.add_argument('--png', help='write map to png', action="store_true")
parser.add_argument('--csv', help='write plots inventory to csv]', action="store_true")
args = parser.parse_args()

sp_name = args.species
buff_dist = args.buff
shp = args.shp
png = args.png
csv = args.csv

# sp_name = 'Calluna vulgaris'
# buff_dist = 25000
# shp = False
# png = True

'''Pointers to source data'''
eva_species_db = r"c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\5_ev" \
                 r"a_species\eva_doren_species_selected_200422-1732.csv"
eva_header_db = r"c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\4_EV" \
                r"A_POSCH_Covs_Eobs\eva_headers_w_NDep_MAX_CoVars_Eobs20200421-1824.pkl"
base_out_dir = r'c:\apps\proj_code\doren_2019\c_out'
aoi_shp = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\geodata\eva_aoi_fin_3035.shp'
background_shp = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\geodata\europe_3035.shp'

"""read as pandas db"""
s_db = pd.read_csv(eva_species_db, sep=';')  # read species database
h_db = pd.read_pickle(eva_header_db)  # read header database

# set 3035 coordinates as geometry
h_db['plot_3035'] = h_db.plot_coordinates_3035.apply(shapely.geometry.Point)
h_db.set_geometry('plot_3035', inplace=True)

"""header data of plots containing the species"""
assert sp_name in set(s_db.species_name_hdr), 'Species {0} not found'.format(sp_name)
plots_w_species = set(s_db.plot_obs_id[s_db.species_name_hdr == sp_name])
plots_wo_species = h_db.index.difference(plots_w_species)

print('Found {0} plots w. {1} out of {2}'.format(len(plots_w_species), sp_name, h_db.shape[0]))

"""create buffers around each selected veg plot"""
buffers = [shapely.geometry.Point(p.x, p.y).buffer(buff_dist) for p in h_db.loc[plots_w_species].geometry]
buff = shapely.ops.unary_union(buffers)  # Using shapely directly instead of GeoPandas, neat!
buff_gdf = gp.GeoDataFrame(pd.DataFrame({'ID': 1, 'soort': sp_name, 'geometry': buff}, index=pd.RangeIndex(len(buff))))

"""find plots within the buffer"""
nearby_plots = gp.sjoin(left_df=h_db.loc[plots_wo_species], right_df=buff_gdf, op='within', how='inner').index
assert set(nearby_plots) <= set(plots_wo_species)
print('Found {0} plots w/o {1} within {2}m radius of {3} positive plots'.format(len(nearby_plots), sp_name, buff_dist,
                                                                                len(plots_w_species)))

"""write to files"""
timestamp_brief = datetime.datetime.now().strftime("%Y%m%d")
timestamp_full = datetime.datetime.now().strftime("%Y%m%d%H%M")
out_dir = os.path.join(base_out_dir, 'Buffers_{0}'.format(timestamp_brief))
if not os.path.isdir(out_dir):
    os.mkdir(out_dir)
basename = '{0}_{1}_{2}m'.format(timestamp_full, sp_name, buff_dist)

# figure
if png:

    # output dir
    png_dir = os.path.join(out_dir, 'png')
    if not os.path.isdir(png_dir):
        os.mkdir(png_dir)

    # read additional shapefiles
    background = gp.read_file(background_shp)
    aoi = gp.read_file(aoi_shp)

    # set the canvas
    fig = plt.figure(figsize=(8,10))
    ax = fig.add_subplot(111)
    ax.set(xlim=[2448000, 5427000], ylim=[1884000, 4888000])
    ax.set_aspect('equal')
    plt.tick_params(axis='both', labelbottom=False, labeltop=False, labelleft=False, labelright=False)

    # plot layers
    background.plot(ax=ax, facecolor='#729b6f', edgecolor="none")  # background countries
    aoi.plot(ax=ax, facecolor='#2d2c2c', alpha=0.3, edgecolor="none")  # AOI
    buff_gdf.boundary.plot(ax=ax, edgecolor='#232323')  # buffers
    h_db.loc[nearby_plots].plot(ax=ax, marker='o', color='#ff9e17', markersize=2, legend=True)  # nearby plots
    h_db.loc[plots_w_species].plot(ax=ax, marker='o', color='#be2f00', markersize=2)  # plots with species
    background.boundary.plot(ax=ax, edgecolor='#365733')

    # legend
    legend_patches = [mpatches.Patch(label='Resident plots, n={:,}'.format(len(plots_w_species)),
                                     facecolor='#be2f00'),
                      mpatches.Patch(label='Nearby plots, n={:,}'.format(len(nearby_plots)),
                                     facecolor='#ff9e17'),
                      mpatches.Patch(label='Buffers. Size={:,}m'.format(buff_dist), edgecolor='#232323',
                                     facecolor='none')]
    plt.legend(handles=legend_patches, loc='upper left', fontsize='small', frameon=True, title=sp_name)

    # save to png
    fig.savefig(os.path.join(out_dir, '{0}.png'.format(basename)))
    plt.close()

# shapefiles
if shp:
    shp_dir = os.path.join(out_dir, 'shp')
    if not os.path.isdir(shp_dir):
        os.mkdir(shp_dir)

    buff_gdf.to_file(os.path.join(shp_dir, '{0}_{1}mBuffer.shp'.format(basename, buff_dist)))
    h_db.loc[plots_w_species].drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', 'plot_pnt_wgs84'], axis=1) \
                             .to_file(os.path.join(shp_dir, '{0}_resident_plots.shp'.format(basename)))
    h_db.loc[nearby_plots].drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', 'plot_pnt_wgs84'], axis=1) \
                          .to_file(os.path.join(shp_dir, '{0}_nearby_plots.shp'.format(basename)))

# plot species info as csv
if csv:
    csv_dir = os.path.join(out_dir, 'csv')
    if not os.path.isdir(csv_dir):
        os.mkdir(csv_dir)

    sp_sp = set(s_db.loc[s_db["species_name_hdr"] == sp_name, 'plot_obs_id'])
    out_w_species = s_db.loc[s_db.species_name_hdr == sp_name, ['plot_obs_id', 'cover_perc']]
    out_wo_species = pd.DataFrame(data={'plot_obs_id': list(nearby_plots), 'cover_perc':0})

    eva_species_out = pd.concat([out_w_species, out_wo_species])
    eva_species_out.to_csv(os.path.join(csv_dir, '{0}_species.csv'.format(basename)), sep=';', index=False)

    '''
    # all plots with *species* and the nearby plots.
    output_plots = plots_w_species.union(nearby_plots)
    eva_headers_out = h_db.loc[output_plots]  # header data
    eva_species_out = s_db.loc[s_db.plot_obs_id.isin(list(output_plots)), ['plot_obs_id', 'species_nr']]  #species data

    eva_headers_out.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', "plot_pnt_wgs84"], axis=1)\
                   .to_csv(os.path.join(csv_dir, '{0}_headers.csv'.format(basename)), sep=';', index=True)

    eva_species_out.to_csv(os.path.join(csv_dir, '{0}_species.csv'.format(basename)), sep=';', index=False)
'''