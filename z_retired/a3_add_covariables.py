"""
Adding co-variables to the EVA-POSCH dataset based on geospatial rasters
Hans Roelofsen, WEnR, april 2020
"""

import os
import datetime
import pickle
import pandas as pd
from utils import doren as do

"""Starting data"""
eva_compiled_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\2_EVA_Posch'
eva_in = 'eva_headers_w_NDep_MAX_20200409-1448.pkl'
eva = pd.read_pickle(os.path.join(eva_compiled_dir, eva_in))
eva_pre = eva.shape[0]

"""Output data"""
out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\3_EVA_POSCH_Covs'
datestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M")
basename = 'eva_headers_w_NDep_MAX_CoVars{0}'.format(datestamp)

"""Covariable data"""
covs = {'WRBLEV1_laea': {"dir": r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\covariables\soil\b_processed',
                         "in": "WRBLEV1_laea.tif"}}
msgs = []

# TODO: landcode!

for covar in covs.keys():
    """Add covariable 1. Change to for loop for all covariables later"""
    # sample the raster for values. Use LAEA EPSG::3035 CRS
    cov = do.get_raster_vals(coords=eva.plot_coordinates_3035, rast_src=os.path.join(covs[covar]['dir'],
                                                                                     covs[covar]['in']), nominal=True)
    # TODO: vals and cats are now the returned columnnames
    # Drop NAs, meaning plots outside the raster and 'weg' which are unwanted categories
    nas = cov.loc[cov["vals"].isna(), :].index
    wegs = cov.loc[cov["cats"] == 'weg', :].index

    cov = cov.loc[cov.index.difference(nas.union(wegs))]

    msg = 'Covariable {0} based on {1} yields {2} values:\n  {3} starting\n  -{4} outside the raster\n  ' \
          '-{5} with invalid category\n\n'.format(covar, covs[covar]['in'], cov.shape[0], eva.shape[0], len(nas),
                                                 len(wegs))
    msgs.append(msg)
    print(msg)

    eva = eva.join(other=cov, how='inner')

eva_post = eva.shape[0]

'''Save to files'''
file_desc = 'File {0}.* contains {1} EVA plot header data. Starting from {2} with {3} plots, the following ' \
            'co-variables were added as additional columns:\n\n'.format(basename, eva_post, eva_in, eva_pre)
# Text metadata
source = 'See git for source script: https://git.wur.nl/roelo008/doren_2019'
footer = '\nMade with Python 3.5 using Pandas by Hans Roelofsen, WEnR team B&B, dated {0}'.format(datestamp)
with open(os.path.join(out_dir, '{0}.txt'.format(basename)), 'w') as f:
    f.write(file_desc)
    f.write(''.join(msgs))
    f.write(source)
    f.write(footer)

# to CSV
eva.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', "plot_pnt_wgs84"], axis=1).\
    to_csv(os.path.join(out_dir, '{0}.csv'.format(basename)), sep=';')
"""NOTE: coordinate tuples are stored as string and cannot be recovered as tuple! """

# To pickle
with open(os.path.join(out_dir, '{0}.pkl'.format(basename)), 'wb') as handle:
    pickle.dump(eva, handle)

# To Shapefile
eva.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035'], axis=1) \
       .to_file(os.path.join(out_dir, '{0}.shp'.format(basename)))
