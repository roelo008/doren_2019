'''
Join EMEP deposition data to EVA vegetation plots

Hans Roelofsen, WEnR, team B&B, 16 sept 2019

'''

import os
import pickle
import pandas as pd
import datetime

'''
Read EVA vegetation data and EMEP N deposition data
'''
eva_source = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\1_EVA\eva_header_selection_20200318-1511.pkl'
emep_source = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\2_emep\emep_qndep_prepared_200318-1526.pkl'

with open(eva_source, 'rb') as handle:
    eva = pickle.load(handle)

with open(emep_source, 'rb') as handle:
    emep = pickle.load(handle)

'''
Create key combining year of observation/EMEP year with EMEP tile. Join.
'''
eva['key'] = eva.apply(lambda row: '{0}_{1}'.format(row.emep_tile_, row.year), axis=1)
emep['key'] = emep.apply(lambda row: '{0}_{1}'.format(row.tile_id, row.year), axis=1)

eva_emep = pd.merge(left=eva, right=emep, how='inner', left_on='key', right_on='key', suffixes=('eva_', 'emep_'))

print('Reading {0} EVA opnames. {1} remaining after coupling with EMEP based on location '
      'and year.'.format(eva.shape[0], eva_emep.shape[0]))

'''
sanity check
'''
check1 = any(eva_emep.ndep_tot.isna())
check2 = any(eva_emep.emep_tile_.isna())
if any([check1, check2]):
    raise Exception('Something is wrong')

'''
Write to various formats
'''
datestamp = datetime.datetime.now().strftime("%y%m%d-%H%M")
out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\3_eva_emep'

with open(os.path.join(out_dir, 'eva_emep_gdf_{0}.pkl'.format(datestamp)), 'wb') as handle:  # pickle
    pickle.dump(eva_emep, handle)

eva_emep.drop(labels=["plot_coordinates_wgs84", 'plot_coordinates_3035'], axis=1) \
        .to_file(os.path.join(out_dir, 'shp', 'eva_emep_gdf_{0}.shp'.format(datestamp)))  # shapefile

eva_emep.drop(labels=["plot_coordinates_wgs84", 'plot_pnt_wgs84', 'plot_coordinates_3035'], axis=1)\
        .to_csv(os.path.join(out_dir, 'eva_emep_gdf_{0}.csv'.format(datestamp)), sep=';', index=False,
                encoding='utf-16')

'''
with open(os.path.join(out_dir, 'eva_emep_gdf_{0}.csv'.format(datestamp)), 'w') as f:
    f.write('#Extract from EVA vegetation database with EMEP N deposition data attached.\n#N dep is coupled to each '
            'plot via its spatial location, using correspondence between EMEP edition and year of the vegetation '
            ' plot.\n#N dep columns are: noxn_dry, oxn_wet, rdn_dry, rdn_wet, ndep_tot.\n#Created {0} by Hans Roelofsen'
            'WEnR team B&B using Python (pandas, geopandas), '
            'see:  https://git.wur.nl/roelo008/doren_2019\n'.format(datestamp))
    f.write(eva_emep.drop(labels=["plot_coordinates_wgs84", 'plot_pnt_wgs84', 'plot_coordinates_3035'], axis=1)
                    .to_csv(sep=';', index=False, encoding='utf-16'))


eva_emep.drop('plot_coordinates', axis=1).to_csv(,
                                                 sep=';', index=False)
'''