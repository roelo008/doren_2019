'''
Compile EMEP N deposition data for use
Hans Roelofsen, WEnR B&B, 16 september 2019
DOREN project
'''

import os
import datetime
import pandas as pd
import pickle

emep_full = pd.read_csv(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\EMEP\QNdepAll.txt',
                        sep=';', comment='#')

# rename columns
colnames = ["i", "j", "Yr", "OXN_D", "OXN_W", "RDN_D", "RDN_W", "NdepTot"]
colnames_n = ['i', 'j', 'year', 'oxn_dry', 'oxn_wet', 'rdn_dry', 'rdn_wet', 'ndep_tot']
emep_full.rename(columns=dict(zip(colnames, colnames_n)), inplace=True)

# fix datatype year column
emep_full.year = pd.to_numeric(emep_full.year, errors='coerce', downcast='integer')

# calculate tile id
emep_full['tile_id'] = emep_full.apply(lambda row: '{0}_{1}'.format(int(row.i), int(row.j)), axis=1)

# drop NAs
emep_sel = emep_full.dropna(axis=0)

# report
msg = 'Origineel {0} rows, {1} rows remaining after dropping NAs'.format(emep_full.shape[0], emep_sel.shape[0])
print(msg)
print(emep_sel.head())

# write to file
out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\2_emep'
datestamp = datetime.datetime.now().strftime("%y%m%d-%H%M")
with open(os.path.join(out_dir, 'emep_qndep_prepared_{0}.pkl'.format(datestamp)), 'wb') as handle:
    pickle.dump(emep_sel, handle)

with open(os.path.join(out_dir, 'emep_qndep_prepared_{0}.txt'.format(datestamp)), 'w') as f:
    f.write(msg)
