"""
Script for adding Temperature and Precipitation historic data to vegetation plots. Based on EObs data prepared in
C:\apps\proj_code\doren_2019\utils\prep_EObs.py

Hans Roelofsen, WEnR, 21 april 2020
"""

import os
import datetime
import pandas as pd
import numpy as np
import pickle
from utils import doren as do

"""Starting data"""
eva_compiled_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\3_EVA_POSCH_Covs'
eva_in = 'eva_headers_w_NDep_MAX_CoVars20200409-1507.pkl'
eobs_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\covariables\EObs\2_compiled'
eobs_basename = 'EObs_v200e_{0}_5yrmean{1}'

"""Output data"""
out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\4_EVA_POSCH_Covs_Eobs'
datestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M")
basename = 'eva_headers_w_NDep_MAX_CoVars_Eobs{0}'.format(datestamp)

'''Read source data'''
eva = pd.read_pickle(os.path.join(eva_compiled_dir, eva_in))
eva_pre = eva.shape[0]

'''Sample values from EObs raster corresponding to the year of the vegetation plot.'''
yrs = set(eva.year)

for yr in yrs:
    temp_src = eobs_basename.format('tg', yr)
    prec_src = eobs_basename.format('rr', yr)

    # Plots in year yr
    sel_eva = eva.loc[eva['year'] == yr, :].index

    print('Now sampling {0} EVA plots in year {1} for temp and '
          'precipitation from {2} and {3}.'.format(len(sel_eva), yr, temp_src, prec_src))

    # sample the rasters
    temp = do.get_raster_vals(coords=eva.loc[sel_eva, 'plot_pnt_wgs84'],
                              rast_src=os.path.join(eobs_dir, '{0}.tif'.format(temp_src)))
    prec = do.get_raster_vals(coords=eva.loc[sel_eva, 'plot_pnt_wgs84'],
                              rast_src=os.path.join(eobs_dir, '{0}.tif'.format(prec_src)))

    # update eva dataframe
    eva.loc[sel_eva, 'EObs_tg'] = temp.loc[:, '{0}_vals'.format(temp_src)]
    eva.loc[sel_eva, 'EObs_rr'] = prec.loc[:, '{0}_vals'.format(prec_src)]


eva.dropna(axis=0, subset=['EObs_tg', 'EObs_rr'], how='any', inplace=True)
eva_post = eva.shape[0]
print('{0} out of {1} EVA records remaining after adding temperature and precipitation'.format(eva_post, eva_pre))

'''Save to files'''
file_desc = 'File {0}.* contains {1} EVA plot header data. Starting from {2} with {3} plots, EObs temperature ' \
            'and precipitation data were added as two new columns. {4} plots were dropped because EObs did not ' \
            'provide values at these locations.\n'.format(basename, eva_post, eva_in, eva_pre,
                                                          np.subtract(eva_pre, eva_post))
# Text metadata
source = 'See git for source script: https://git.wur.nl/roelo008/doren_2019'
footer = '\nMade with Python 3.5 using Pandas by Hans Roelofsen, WEnR team B&B, dated {0}'.format(datestamp)
with open(os.path.join(out_dir, '{0}.txt'.format(basename)), 'w') as f:
    f.write(file_desc)
    f.write(source)
    f.write(footer)


# to CSV
eva.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', "plot_pnt_wgs84"], axis=1).\
    to_csv(os.path.join(out_dir, '{0}.csv'.format(basename)), sep=';')

# To pickle
with open(os.path.join(out_dir, '{0}.pkl'.format(basename)), 'wb') as handle:
    pickle.dump(eva, handle)

# To Shapefile
eva.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035'], axis=1) \
       .to_file(os.path.join(out_dir, '{0}.shp'.format(basename)))
