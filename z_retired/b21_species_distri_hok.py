"""
Select and show plots containing species X on a map. Also map plots residing within buffer distance of selected plots

Hans Roelofsen, WEnR, team BB, jan 2020
"""

import os
import datetime
import pandas as pd
from utils import doren as do

'''input parameter'''
sp_name = 'Calluna vulgaris'
eva_species_db = r'\\wur\dfs-root\PROJECTS\Doren19\b_compiled_data\eva_emep\eva_doren_species_selected_191002-1059.csv'
eva_header_db = r'\\wur\dfs-root\PROJECTS\Doren19\b_compiled_data\EVA\eva_header_prepared_200130-1520.pkl'
base_out_dir = r'c:\apps\proj_code\doren_2019\c_out'
sq_size = 25000

# read as pandas db
s_db = pd.read_csv(eva_species_db, sep=';')  # read species database
h_db = pd.read_pickle(eva_header_db)  # read header database

# calculate square IDs based on sq size for the header data
h_db['square_id'] = h_db.loc[:, 'plot_coordinates_3035'].apply(do.generate_square_id, size=sq_size)

# header data of plots containing the species
plots_w_species = do.query_eva_for_species(s_db, h_db, sp_name)

# selected + all other plots in selected squares
nearby_plots = h_db.loc[h_db['square_id'].isin(list(set(plots_w_species.square_id))), :]

# squares as geodatabase
squares = do.generate_square_shapes(plots_w_species.square_id.tolist(), sq_size)

# write to shapefiles
timestamp_brief = datetime.datetime.now().strftime("%Y%m%d%H%M")
out_dir = os.path.join(base_out_dir, '{0}{1}'.format(timestamp_brief, sp_name))
os.mkdir(out_dir)

basename = '{0}_{1}'.format(timestamp_brief, sp_name)
# TODO: set 3035 coordinates as geometry and specify CRS in geopandas to_file()
plots_w_species.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035'], axis=1) \
               .to_file(os.path.join(out_dir, '{0}_resident_plots.shp'.format(basename)))
nearby_plots.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035'], axis=1) \
            .to_file(os.path.join(out_dir, '{0}_nearby_plots.shp'.format(basename)))
squares.to_file(os.path.join(out_dir, '{0}_{1}n-squares.shp'.format(basename, sq_size)))
