'''
Script to check equality contents of

1) d:\DOREN19\b_compiled_data\eva_emep\eva_emep_gdf_190918-1612.pkl
2) d:\DOREN19\b_compiled_data\eva_emep\eva_emep_gdf_190918-1241.csv

Conclusion, they are not identical, but very likely contain the same information. Both can be used.


Hans Roelofsen, 7 oktober 2019, WEnR team B&B
'''

import pandas as pd

df1_1612 = pd.read_pickle(r'd:\DOREN19\b_compiled_data\eva_emep\eva_emep_gdf_190918-1612.pkl')
df2_1241 = pd.read_csv(r'd:\DOREN19\b_compiled_data\eva_emep\eva_emep_gdf_190918-1241.csv',
                       sep=';', comment='#')

try:
    assert_df_equal = pd.testing.assert_frame_equal(left=df1_1612, right=df2_1241, check_names=False)
    print(True)
except AssertionError:
    print(False )

# colnames
set(list(df1_1612)) - set(list(df2_1241))  # colnames in df1, not in df2: plot_coordinates

# types
type(df1_1612)  # GeoDataframe
type(df2_1241)  # Pandas dataframe

# shape
print(df1_1612.shape)  ## 672.881, 31
print(df2_1241.shape)  ## 672.881, 31

# hash plot_obs_id column
h1 = pd.util.hash_pandas_object(df1_1612.plot_obs_id, index=False)
h2 = pd.util.hash_pandas_object(df2_1241.plot_obs_id, index=False)
if pd.testing.assert_series_equal(left=h1, right=h2):
    print('plot_obs_id columns are identical')
else:
    print('plot_obs_id columns are not identical')