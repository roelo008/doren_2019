"""
Script for grabbing the NDep data from MAX POSCH and adding to the EVA header data
Hans Roelofsen, 01-04-2020

New colums to EVA headers are:
1) nh3 in mg/m2
2) nox in mg/m2
3) totale Ndep in mg/m2 (nh3 + nox)
4) totale Ndep in kg/ha
5) totale NDep in molN/ha (N [kg/ha] / 14)

EVA data and MAX POSCH data contain identical rows (EVA veg) plots. Use year column from Max Posch corresponding to
plot year.

"""

import pandas as pd
import datetime
import os
import pickle

from utils import doren as do

'''Source data'''
eva_compiled_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\1_EVA'
eva_compiled_in = 'eva_header_selection_20200409-1437.pkl'

ndp_compiled_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\POSCH_dep'
nh3_in = 'NH3-20200331.csv'
nox_in = 'NOx-20200331.csv'

'''Output data'''
out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\2_EVA_Posch'
datestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M")
basename = 'eva_headers_w_NDep_MAX_{0}'.format(datestamp)

eva = pd.read_pickle(os.path.join(eva_compiled_dir, eva_compiled_in))
eva['year'] = eva.apply(lambda row: do.int_year(row.date_of_recording), axis=1)

nh3 = pd.read_csv(os.path.join(ndp_compiled_dir, nh3_in), sep=',', comment='!', index_col='plot_obs_id')
nox = pd.read_csv(os.path.join(ndp_compiled_dir, nox_in), sep=',', comment='!', index_col='plot_obs_id')
yr_cols = [x for x in list(nh3) if x.isdigit()]
nh3.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)
nox.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)

'''Add columns 1 and 2
https://stackoverflow.com/questions/60976021/pandas-select-from-column-with-index-corresponding-to
-values-in-another-column/
'''
#NH3
mapper = eva.assign(year=('year_' + eva['year'].astype(str)), plot_obs_id=eva.index)
c2 = mapper['plot_obs_id'].isin(nh3.index)
c1 = mapper['year'].isin(nh3.columns)
mapper = mapper.loc[c1 & c2]
eva.loc[c2 & c1, 'nh3_mg/m2'] = nh3.lookup(mapper['plot_obs_id'], mapper['year'])
eva['nh3_mg/m2'] = eva['nh3_mg/m2'].fillna(-99)

# NOX
mapper = eva.assign(year=('year_' + eva['year'].astype(str)), plot_obs_id=eva.index)
c2 = mapper['plot_obs_id'].isin(nox.index)
c1 = mapper['year'].isin(nox.columns)
mapper = mapper.loc[c1 & c2]
eva.loc[c2 & c1, 'nox_mg/m2'] = nox.lookup(mapper['plot_obs_id'], mapper['year'])
eva['nox_mg/m2'] = eva['nox_mg/m2'].fillna(-99)

'''Calculate total N in various units'''
eva['totN_mg/m2'] = eva.loc[:, ["nh3_mg/m2", "nox_mg/m2"]].sum(axis=1)
eva['totN_kg/ha'] = eva.loc[:, 'totN_mg/m2'].divide(100)
eva['totN_mol/ha'] = eva.loc[:, 'totN_kg/ha'].divide(14)

'''Save to files'''
# Text metadata
file_desc = 'File {0} contains EVA plot header selection based on {1}, with N Dep data from MAX POSCH (source 1: {2}' \
          ', source 2: {3}) added as additional columns.'.format(basename, eva_compiled_in, nh3_in, nox_in)
source = 'See git for source script: https://git.wur.nl/roelo008/doren_2019'
footer = '\nMade with Python 3.5 using Pandas by Hans Roelofsen, WEnR team B&B, dated {0}'.format(datestamp)
with open(os.path.join(out_dir, '{0}.txt'.format(basename)), 'w') as f:
    f.write(file_desc)
    f.write(source)
    f.write(footer)

# to CSV
eva.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', "plot_pnt_wgs84"], axis=1).\
    to_csv(os.path.join(out_dir, '{0}.csv'.format(basename)), sep=';')
"""NOTE: coordinate tuples are stored as string and cannot be recovered as tuple! """

# To pickle
with open(os.path.join(out_dir, '{0}.pkl'.format(basename)), 'wb') as handle:
    pickle.dump(eva, handle)

# To Shapefile
eva.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035'], axis=1) \
       .to_file(os.path.join(out_dir, '{0}.shp'.format(basename)))
