"""
Script to compile raw EVA header data into workable selectie

Hans Roelofsen, WEnR B&B, 16 sept 2019
"""

import numpy as np
import pandas as pd
import geopandas as gp
import datetime
import os
import pickle
import shapely

from utils import doren as do

'''Source data'''
eva_full_scr = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\EVA\EVA_Doren_header.csv'
aoi_src = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\AOI\eva_aoi_fin.shp'
emep_src = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\EMEP\cell_shp\emep_cells.shp'
eva_species_src = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\a_brondata\EVA\EVA_Doren_species.csv'
dem_src = r'c:\apps\temp_geodata\doren\DEM\DTM_3035.tif'

'''Output data'''
out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\1_EVA'
datestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M")
basename = 'eva_header_selection_{0}'.format(datestamp)

'''Read original and remap columns to sensible format. 
Drop columns with tricky characters and/or not so usefull information'''
eva_full = pd.read_csv(eva_full_scr, comment='#', sep='\t', low_memory=False, index_col='PlotObservationID')
eva_full.rename(columns=dict(zip(do.eva_colnames_orig(), do.eva_colnames_new())), inplace=True)
eva_full.drop(labels=do.eva_colnames_drop(), axis=1, inplace=True)

'''Read species info'''
eva_spec = pd.read_csv(eva_species_src, sep='\t')

'''Prepare AOI from provided shapefile or use world extent instead'''
if aoi_src:
    aoi = do.read_aoi_from_src(aoi_src)
else:
    aoi = do.get_world_aoi_gdf()

'''
Check data column types: repair longitude to a float. Note: coerce = set to Null
Create new columns: 
  top-level EUNIS type
  vegetation tye gras, bos, heide based on EUNIS type, using json dict
  year as integer
  elevation based on dem_src
  geometery and convert to GeoDataFrame
'''
eva_full.longitude = pd.to_numeric(eva_full.longitude, errors='coerce', downcast='float')
eva_full['eunis_top'] = eva_full.apply(lambda row: do.first_letter(row.eunis), axis=1)
eva_full['veg_type'] = eva_full.apply(lambda row: do.eunis_to_veg(row.eunis), axis=1)
eva_full['year'] = eva_full.apply(lambda row: do.int_year(row.date_of_recording), axis=1)
eva_full['plot_coordinates_wgs84'] = list(zip(eva_full.longitude, eva_full.latitude))
eva_full['plot_coordinates_3035'] = eva_full.loc[:, 'plot_coordinates_wgs84'].apply(do.lon_lat_2_east_north)
eva_full['plot_pnt_wgs84'] = eva_full['plot_coordinates_wgs84'].apply(shapely.geometry.Point)
eva_full = gp.GeoDataFrame(eva_full, geometry='plot_pnt_wgs84', crs={"init": "epsg:4326"})

'''Optional requirements to which the data must comply'''
req1_desc = "Req 1: <latitude> is niet leeg"
req2_desc = "Req 2: <longitude> is niet leeg"
req3_desc = 'Req 3: plot is in AOI {0}'.format(aoi_src)
req4_desc = 'Req 4: Altitude <= 500 m based on {0}'.format(dem_src)
req5_desc = 'Req 5: Altitude is niet leeg'
req6_desc = 'Req 6: <eunis> is niet "?"'
req7_desc = 'Req 7: <eunis> is niet leeg'
req8_desc = "Req 8: <date_of_recording> is niet leeg"
req9_desc = "Req 9: <date_of_recording> is jonger dan 1950"
req10_desc = "Req 10: plot has species inventory"

'''Which rows do NOT meet requirements?'''
req1 = eva_full.loc[eva_full.latitude.isna(), :].index
req2 = eva_full.loc[eva_full.longitude.isna(), :].index
req3 = eva_full.index.difference(gp.sjoin(left_df=eva_full, right_df=aoi, how='inner', op='within',
                                 lsuffix='eva_', rsuffix='aoi_').index)
req4 = do.get_raster_vals(coords=eva_full.plot_coordinates_3035.loc[eva_full.index.difference(req1.union(req2))],
                          rast_src=dem_src).query('vals.isna() or vals > 500').index
req5 = eva_full.loc[eva_full.altitude_m.isna(), :].index
req6 = eva_full.loc[(eva_full['eunis'] == '?'), :].index
req7 = eva_full.loc[eva_full.eunis.isna(), :].index
req8 = eva_full.loc[eva_full.date_of_recording.isna(), :].index
req9 = eva_full.loc[eva_full['year'] < 1950, :].index
req10 = eva_full.index.difference(set(eva_spec.PlotObservationID))

'''Apply wich requirements? This should be converted to optional arguments in argparse sometime.'''
reqs = ["req1", "req2", "req3", "req4", "req8", 'req9', 'req10']

'''Create union of rows that DO NOT meet requirements and drop rows'''
set_drop_rows = set().union(*[eval(x) for x in reqs])
eva_sel = eva_full.drop(labels=list(set_drop_rows))

''''Prepare log for reporting'''
header = 'EVA header from {0} with {1} original rows. ' \
         'The following selection requirements were applied\n'.format(eva_full_scr, eva_full.shape[0])
msgs = [do.query_msg(x, y) for x, y in zip([eval(x) for x in ['{0}_desc'.format(x) for x in reqs]],
                                           [eval(x) for x in reqs])]
footer = 'Union between requirements {0} gives {1} rows, leaving {2}-{1} = {3} EVA records.'\
    .format(', '.join(reqs), len(set_drop_rows), eva_full.shape[0], np.subtract(eva_full.shape[0], len(set_drop_rows)))
print(header, *msgs, footer)

'''Save to files'''
# Text metadata
with open(os.path.join(out_dir, '{0}.txt'.format(basename)), 'w') as f:
    f.write(header)
    f.write(''.join(msgs))
    f.write(footer)
    f.write('\nMade with Python 3.5 using Pandas by Hans Roelofsen, WEnR team B&B, dated {0}'.format(datestamp))

# to CSV
eva_sel.to_csv(os.path.join(out_dir, '{0}.csv'.format(basename)), sep=';')
"""NOTE: coordinate tuples are stored as string and cannot be recovered as tuple! """

# To pickle
with open(os.path.join(out_dir, '{0}.pkl'.format(basename)), 'wb') as handle:
    pickle.dump(eva_sel, handle)

# To Shapefile
eva_sel.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035'], axis=1) \
       .to_file(os.path.join(out_dir, '{0}.shp'.format(basename)))
