#!/bin/bash
#SBATCH --comment=773320000
#SBATCH --time=1-0
#SBATCH --mem-per-cpu=4000
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --nodes=1
#SBATCH --array=0-49
#SBATCH --output=./out/%A_%a_output.txt
#SBATCH --error=./out/%A_%a_error_output.txt
#SBATCH --job-name=03051226
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hans.roelofsen@wur.nl

# activate conda environment
source /home/WUR/roelo008/miniconda3/etc/profile.d/conda.sh
conda activate rasters

echo $SLURM_ARRAY_TASK_ID

# Run
cd /home/WUR/roelo008/projs/doren_2019
python run_species.py $SLURM_ARRAY_TASK_ID 50 doren_20210113.pkl