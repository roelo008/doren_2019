"""
Classes for holding and doing everything DOREN related
Hans Roelofsen, WEnR, 28-04-2020
"""

import os
import pandas as pd
import numpy as np
import geopandas as gp
import datetime
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import shapely

from utils import doren as do

class Doren:
    """
    Class holding Pandas dataframe for
    1. plot headers, containing all side data related to the vegetation plots. Each line = one plot
    2. plot species inventory, containing all species found in each plot. Each line = 1 species in 1 plot:
       plot 1: species x
       plot 1: species y
       plot 1: species z
       plot 2: species y
       plot 2: species q
       ....
       plot n: species z

    """

    def __init__(self, header_src, sp_src, verbose=True):
        self.header_src = header_src  # path to EVA header source data
        self.sp_src = sp_src          # path to EVA species source data
        self.eva = None               # holder for header df
        self.spec = None              # holder for species df
        self.species = None           # all unique species
        self.species2nr = None        # dictionary mapping species name to number
        self.nr2species = None        # reversed dictionary
        self.weird_species = None     # species non complying to expected format
        self.positive_plots = None    # plot IDs for plots containing a certain species
        self.negative_plots = None    # plot IDs for plots not containing a certain species
        self.nearby_plots = None      # plot IDs for plots within Xm buffer of self.positive_plots
        self.sel_sp_coverage = None   # Series of coverage of selected species in the positive plots
        self.buffer_gdf = None        # holder for geodataframe w. buffers
        self.sel_species = None       # holder for a species
        self.sel_species_ascii = None # holder for a species name with ascci encondig
        self.buffer_size = None       # holder for buffer size
        self.structuurtypes = None    # holder for list of structuurtypes. Must be list, not set, for ordering!
        self.sample = False           # Dataset is a random sample of all plots, for testing purposes.
                                      # relevant stats
        self.status = {'n_plots': 0, 'columns': [],  'n_species': 0, 'covars': []}
        self.basename = 'DOREN'       # basename for all output
        self.verbose = verbose        # boolean, report on progress?
        self.report = ''              # tracking everything in a report
        self.request_df = None        # df with all requested species
        self.req_sp = None            # species requested for processing from external source
        self.req_found = None         # subset of req_sp found in self.species
        self.req_not_found = None     # subset of req_sp not found in self.species
        self.req_not_found_df = None  # dataframe with as above plus alternatives that are found
                                      # base directory for all output
        self.base_out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\z_scratch'
                                      # source to shapefile with background images
        self.background_shp = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\geodata\europe_3035.shp'
        self.aoi_shp = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\geodata\eva_aoi_fin_3035.shp'
        self.timestamp = datetime.datetime.now().strftime("%Y%m%d")
        self.timestamp_full = datetime.datetime.now().strftime("%Y%m%d%H%M")

    def initiate(self, sample=False, species_col='matched_concept'):
        """
        Read source data, sanitize column names and fix some datatypes
        :return: self.eva and self.species populated with (geo)dataframes
        """

        # Use SAMPLE data if requested
        if sample:
            self.sample = sample
            self.header_src = r'w:\PROJECTS\Doren19\a_brondata\EVA\delivery_20201118\SAMPLE\EVA_Doren_header_SAMPLE.csv'
            self.sp_src = r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20201118\SAMPLE\EVA_Doren_species_SAMPLE.csv'
            self.report += '\n\n==SAMPLE MODE, CONTENT RESTRICTED TO 40 PLOTS==\n\n'

        # Read EVA species data
        species = pd.read_csv(self.sp_src, sep='\t', memory_map=True)
        colnames_n = ['plot_obs_id', 'taxonomy', 'taxon_group', 'taxon_group_id', 'turboveg2_concept',
                      'matched_concept', 'match', 'layer', 'cover_perc', 'cover_code']
        species.rename(columns=dict(zip(list(species), colnames_n)), inplace=True)

        # Simplify species name to remove subspecies etc
        setattr(self, 'eva_species_col', species_col)
        species['species_name_hdr'] = species.loc[:, self.eva_species_col].astype(str).apply(do.strip_leading_quote)
        species['species_name_hdr'] = species.species_name_hdr.apply(do.simplify_species)
        self.spec = species

        # Unique species as attribute (set) and also as a pd.Series as temporary object applying functions to
        self.species = set(self.spec.species_name_hdr)
        sp_series = pd.Series(list(self.species)).sort_values()
        self.species2nr = {v: i for i, v in enumerate(sp_series)}
        self.nr2species = {v: k for k, v in self.species2nr.items()}

        # Verify species list for species name formatting: "Family species"
        sp_check = sp_series.apply(do.verify_species)
        if not all(sp_check):
            print('  found {0} species not complying to expected format'.format(len(sp_check.loc[sp_check])))
            self.weird_species = sp_series.loc[~sp_check]

        # Read EVA Header data
        # header_src = r"W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20201118\EVA_Doren_header.csv"
        eva = pd.read_csv(self.header_src, comment='#', sep=',', low_memory=False,
                          memory_map=True, quotechar='"',
                          usecols=['PlotObservationID', 'TV2 relevé number', 'Country','Longitude', 'Latitude',
                                   'Date of recording', 'Dataset', 'EUNIS_Old', 'EUNIS_New'])

        self.report += 'Starting @ {0} with {1} EVA headers containing {2} unique ' \
                       'species (based on column {3} after simplification).\n\n' \
            .format(self.timestamp_full, eva.shape[0], len(self.species), self.eva_species_col)

        try:
            eva.set_index('PlotObservationID', drop=False, verify_integrity=True, inplace=True)
        except ValueError:
            pre = eva.shape[0]
            eva.drop_duplicates(subset='PlotObservationID', inplace=True)
            post = eva.shape[0]
            self.report += 'Dropping duplicate PlotObservationsIDs going from {0} to {1}\n\n'.format(pre, post)
            eva.set_index('PlotObservationID', drop=False, verify_integrity=True, inplace=True)
        eva.rename(columns=dict(zip(do.eva_colnames_orig(), do.eva_colnames_new())), inplace=True)

        # Create true coordinates in WGS84 and 3035
        eva.longitude = pd.to_numeric(eva.longitude, errors='coerce', downcast='float')
        eva['plot_coordinates_wgs84'] = list(zip(eva.longitude, eva.latitude))
        eva['plot_point_wgs84'] = [shapely.geometry.Point(x, y) for (x, y) in eva.plot_coordinates_wgs84]
        eva['plot_coordinates_3035'] = eva.plot_coordinates_wgs84.apply(do.lon_lat_2_east_north)

        # Replace "," in composite EUNIS codes
        eva.eunis_old = eva.eunis_old.str.replace(',', '-')
        eva.eunis_new = eva.eunis_new.str.replace(',', '-')

        # Attach as attribute
        self.eva = gp.GeoDataFrame(eva, geometry='plot_coordinates_3035', crs={"init": "epsg:3035"})
        del eva

        self.update_status()
        '''
        Note: Integer kolommen kunnen geen NAs bevatten. Dus procedure zou moeten zijn:
        1. inlezen
        2. selecties en nieuwe kolommen
        3. kolommen parsen naar gewenste datatype 
        '''

    def merge_eunis(self):
        """
        combine old and new EUNIS data to a final EUNIS class
        :return:
        """

        # Assign final EUNIS code using EUNIS_new where possible else EUNIS_old
        use_old_indx = self.eva.loc[(self.eva.eunis_new.isna()) | (self.eva.eunis_new == '?') | (self.eva.eunis_new == 'I')].index
        use_new_indx = self.eva.index.difference(use_old_indx)
        self.eva.loc[use_new_indx, 'eunis_code'] = self.eva.loc[use_new_indx, 'eunis_new']
        self.eva.loc[use_old_indx, 'eunis_code'] = self.eva.loc[use_old_indx, 'eunis_old']
        self.eva.loc[use_new_indx, 'eunis_src'] = 'eunis_new'
        self.eva.loc[use_old_indx, 'eunis_src'] = 'eunis_old'
        self.eva.eunis_code.fillna('?', inplace=True)  # Fill NAs where EUNIS_Old was also NA

    def get_requested_species(self, xls, sheet, col, skip, simplify_names=False):
        """
        read list of species requested for processing
        :param xls: file path to Excel sheet
        :param sheet: sheet name
        :param col: column name
        :return: self.req_sp: all requested species
                 self.req_found: subset of above, found
                 self.req_found: subset of above, not found
        """

        # Read csv file with requested species and drop empty rows
        reqs = pd.read_excel(xls, sheet_name=sheet, skiprows=skip)
        msg1 = 'Read {0} species requested for processing (source: "{1}", ' \
               'sheet: "{2}", column: "{3}").\n'.format(reqs.shape[0], xls, sheet, col)

        pre = reqs.shape[0]
        reqs.dropna(subset=[col], inplace=True, axis=0)
        reqs.drop_duplicates(subset=[col], inplace=True)
        post = reqs.shape[0]
        if post < pre:
            msg1 += 'Dropping {0} duplicates and/or NAs, remaining: {1}\n'.format(pre-post, reqs.shape[0])

        # Optionally simplify species names
        if simplify_names:
            reqs.loc[:, 'species_name_hdr'] = reqs.loc[:, col].astype(str).apply(do.strip_leading_quote)
            reqs.loc[:, 'species_name_hdr'] = reqs.species_name_hdr.astype(str).apply(do.simplify_species)
            reqs.loc[:, col] = reqs.loc[:, 'species_name_hdr']

        # Attach as df
        self.request_df = reqs

        # Update attributes
        req_sp_set = set(self.request_df.loc[:, col])
        self.req_sp = list(req_sp_set)
        self.req_found = self.species.intersection(req_sp_set)  # self.species is set van eva simplified matched_concept
        self.req_not_found = req_sp_set.difference(self.species)
        if simplify_names:
            msg2 = 'Simplyfing requested species names to non-subspecies with {0} remaining.\n'.format(len(req_sp_set))
        else:
            msg2 = 'Now considering {0} species for processing\n'.format(len(self.req_sp))
        msg3 = '  {0} are matching with (simplified) species in DOREN EVA column {1}\n'.format(len(self.req_found),
                                                                                               self.eva_species_col)
        msg4 = '  {0} not matching with (simplified) species in DOREN EVA column {1}\n'.format(len(self.req_not_found),
                                                                                               self.eva_species_col)

        # List alternatives by looking for not-found-species in the 'turboveg2_concept' column instead of
        # 'matched_concept'.
        container = []
        for sp in self.req_not_found:
            alternatives = self.spec.loc[self.spec.turboveg2_concept == sp, 'matched_concept']
            if not alternatives.empty:
                dat = pd.Series(data={sp: list(set(alternatives))})
            else:
                dat = pd.Series(data={sp: 'no alternatives found'})
            container.append(dat)
        self.req_not_found_df = pd.concat(container)

        msg5 = self.req_not_found_df.sort_index().to_csv(sep='\t')
        self.report += msg1 + msg2 + msg3 + msg4 + msg5 + '\n'
        if self.verbose:
            print(msg1, msg2, msg3, msg4, msg5)

    def rename(self, old_name, new_name):
        """
        Rename a species in self.sp and self.species
        :param old_name: species name to be renamed
        :param new_name: new species name
        :return: updated self.sp, self.species
        """
        assert hasattr(self, 'spec'), 'Initiate DOREN object first before messing with plant names'
        pre_count = self.spec.loc[self.spec.species_name_hdr == old_name].shape[0]
        self.spec.species_name_hdr.replace(to_replace=old_name, value=new_name, inplace=True)
        self.update_status()
        post_count_old = self.spec.loc[self.spec.species_name_hdr == old_name].shape[0]
        post_count_new = self.spec.loc[self.spec.species_name_hdr == new_name].shape[0]


    def apply_requirements(self, *reqs, **kwargs):
        """
        Filter down EVA headers by applying one or more requirements
        :param requirements: one or more requirements are 'reqX'
        :param kwargs: source to additional files, if needed
        :return: updated self.eva
        """

        '''Apply wich requirements?'''
        requirements = [req for req in reqs]

        # Additional data sources.
        aoi_src = kwargs.get('aoi_src', None)  # AOI as provided or None
        dem_src = kwargs.get('dem_src', None)  # DEM source

        '''Description of requirements'''
        req_descs = dict(zip(['req{0}'.format(i) for i in range(1, 11)],
                             ["Req 1: <latitude> is niet leeg",
                              "Req 2: <longitude> is niet leeg",
                              'Req 3: plot is in AOI {0}'.format(aoi_src if aoi_src else 'whole Europe.'),
                              'Req 4: Altitude <= 500 m based on {0}'.format(dem_src),
                              'Req 5: Altitude is niet leeg',
                              'Req 6: <eunis> is niet "?"',
                              'Req 7: <eunis> is niet leeg',
                              "Req 8: <date_of_recording> is niet leeg",
                              "Req 9: <date_of_recording> is jonger dan 1950",
                              "Req 10: plot has species inventory"]))

        '''Determine which rows do not meet the requirements'''
        possible_requirements = {}
        if 'req1' in requirements:
            possible_requirements['req1'] = self.eva.loc[self.eva.latitude.isna(), :].index

        if 'req2' in requirements:
            possible_requirements['req2'] = self.eva.loc[self.eva.longitude.isna(), :].index

        if 'req3' in requirements:
            if aoi_src is None:
                print('No AOI src provided, defaulting to entire Europe')
            possible_requirements['req3'] = \
                self.eva.index.difference(gp.sjoin(left_df=self.eva, right_df=do.get_aoi(aoi_src) if aoi_src else do.get_aoi(),
                                                   how='inner',
                                                   op='within', lsuffix='eva_', rsuffix='aoi_').index)

        if 'req4' in requirements:
            # Query all plots on an external DEM for accurate elevation information
            possible_requirements['req4'] = \
                do.get_raster_vals(coords=self.eva.plot_coordinates_3035.loc[self.eva.longitude.notna() &
                                                                             self.eva.latitude.notna()],
                                   rast_src=dem_src).query('vals.isna() or vals > 500').index

        if 'req5' in requirements:
            possible_requirements['req5'] = self.eva.loc[self.eva.altitude_m.isna(), :].index,

        if 'req6' in requirements:
            possible_requirements['req6'] = self.eva.loc[(self.eva['eunis'] == '?'), :].index

        if 'req7' in requirements:
            possible_requirements['req7'] = self.eva.loc[self.eva.eunis_code.isna(), :].index

        if 'req8' in requirements:
            possible_requirements["req8"] = self.eva.loc[self.eva.date_of_recording.isna(), :].index

        if 'req9' in requirements:
            possible_requirements["req9"] = self.eva.loc[self.eva['date_of_recording'] < 1950, :].index

        if 'req10' in requirements:
            possible_requirements["req10"] = self.eva.index.difference(set(self.spec.plot_obs_id))

        '''Create union of rows that DO NOT meet requirements and drop rows'''
        drop_rows = set().union(*[possible_requirements[x] for x in requirements])
        self.eva.drop(labels=list(drop_rows), inplace=True)
        self.update_status()

        '''Messages on requirements'''
        msgs = ['{0} - {1}'.format(x, '{0} rows failed\n'.format(len(y)))
                for x, y in zip([req_descs[x] for x in requirements], [possible_requirements[x] for x in requirements])]
        self.report += 'Applying requirements as follows: \n'
        self.report += ''.join(msgs)
        self.report += 'Union between requirements {0} gives {1} rows that DO NOT meet requirements. ' \
                       'Remaining: {2}\n\n'.format(', '.join(requirements), len(drop_rows), self.status['n_plots'])


    def add_posch_single(self, posch_src_dir):
        """
        Add NDep data from Max Posch from *.csv file where rows are PlotIDs and Columns are years. This function
        is tailored towards the Posch data that are not differentiated between ruwheidsfactor
        :param posch_src_dir: directory containing the source data
        :return: additional columns to self.eva
        """

        # posch_src_dir = r'\\wur\dfs-root\PROJECTS\Doren19\a_brondata\POSCH_dep\20200401delivery'
        try:
            nh3 = pd.read_csv(os.path.join(posch_src_dir, 'NH3-20200505.csv'), sep=',', comment='!',
                                index_col='plot_obs_id')
            nox = pd.read_csv(os.path.join(posch_src_dir, 'NOx-20200505.csv'), sep=',', comment='!',
                                index_col='plot_obs_id')
        except OSError:
            print('Ndeposition data not found in {0}'.format(posch_src_dir))
            raise

        # rename columns to proper format
        yr_cols = [x for x in list(nh3) if x.isdigit()]
        nh3.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)
        nox.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)

        # NH3
        mapper = self.eva.assign(year=('year_' + self.eva.date_of_recording.astype(int).astype(str)),
                                 plot_obs_id=self.eva.index)
        c1 = mapper['plot_obs_id'].isin(nh3.index)
        c2 = mapper['year'].isin(nh3.columns)
        mapper = mapper.loc[c1 & c2]
        self.eva.loc[c1 & c2, 'nh3_mg_m2'] = nh3.lookup(mapper['plot_obs_id'], mapper['year'])

        # NOx
        mapper = self.eva.assign(year=('year_' + self.eva.date_of_recording.astype(int).astype(str)),
                                 plot_obs_id=self.eva.index)
        c1 = mapper['plot_obs_id'].isin(nox.index)
        c2 = mapper['year'].isin(nox.columns)
        mapper = mapper.loc[c1 & c2]
        self.eva.loc[c1 & c2, 'nox_mg_m2'] = nox.lookup(mapper['plot_obs_id'], mapper['year'])

        # Drop NAs
        self.eva.dropna(axis=0, subset=['nh3_mg_m2', 'nox_mg_m2'], how='any', inplace=True)
        self.update_status()

        # Calculate totals
        self.eva['totN_mg_m2'] = self.eva.loc[:, ["nh3_mg_m2", "nox_mg_m2"]].sum(axis=1)
        self.eva['totN_kg_ha'] = self.eva.loc[:, 'totN_mg_m2'].divide(100)
        self.eva['totN_kmol_ha'] = self.eva.loc[:, 'totN_kg_ha'].divide(14)

        # Reporting
        self.update_status(covar=['totN_kmol_ha'])
        msg = '\nAdded NDep Single from POSCH: {0} rows remaining.\n\n'.format(self.eva.shape[0])
        self.report += msg
        if self.verbose:
            print(msg)

    def add_posch_differentiated(self, posch_src_dir):
        '''
        Add N deposition data sourced from Max POSCH. Update November 2020 to differtiate between Ndep in Forests (f)
        and Open vegetation (v). Add three flavours to self.eva: NDep(forest), NDep(Open), NDep(Forest/Open)(afh van structuurtype)
        :param posch_src_dir: directory containing NDep source data
        :return: updated self.eva

        Thanks to https://stackoverflow.com/questions/60976021/pandas-select-from-column-with-index-corresponding-to
         -values-in-another-column/

        '''

        # posch_src_dir = r'\\wur\dfs-root\PROJECTS\Doren19\a_brondata\POSCH_dep\20201012delivery'
        try:
            nh3_f = pd.read_csv(os.path.join(posch_src_dir, 'NH3_f-20201012.csv'), sep=',', comment='!',
                                index_col='plot_obs_id')
            nh3_v = pd.read_csv(os.path.join(posch_src_dir, 'NH3_v-20201012.csv'), sep=',', comment='!',
                                index_col='plot_obs_id')
            nox_f = pd.read_csv(os.path.join(posch_src_dir, 'NOx_f-20201012.csv'), sep=',', comment='!',
                                index_col='plot_obs_id')
            nox_v = pd.read_csv(os.path.join(posch_src_dir, 'NOx_v-20201012.csv'), sep=',', comment='!',
                                index_col='plot_obs_id')
        except OSError:
            print('Ndeposition data not found in {0}'.format(posch_src_dir))
            raise

        assert nh3_f.index.identical(nh3_v.index), 'Mismatch in NH3 NDep data'
        assert nox_f.index.identical(nox_v.index), 'Mismatch in NOx NDep data'

        # rename columns to proper format
        yr_cols = [x for x in list(nh3_f) if x.isdigit()]
        nh3_f.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)
        nh3_v.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)
        nox_f.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)
        nox_v.rename(columns=dict(zip(yr_cols, ['year_{0}'.format(y) for y in yr_cols])), inplace=True)

        # NH3 - Forest to all plots
        mapper = self.eva.assign(year=('year_' + self.eva.date_of_recording.astype(int).astype(str)),
                                 plot_obs_id=self.eva.index)
        c1 = mapper['plot_obs_id'].isin(nh3_f.index)
        c2 = mapper['year'].isin(nh3_f.columns)
        mapper = mapper.loc[c1 & c2]
        self.eva.loc[c1 & c2, 'nh3_mg_m2_f'] = nh3_f.lookup(mapper['plot_obs_id'], mapper['year'])

        # NH3 - Open to all plots
        mapper = self.eva.assign(year=('year_' + self.eva.date_of_recording.astype(int).astype(str)),
                                 plot_obs_id=self.eva.index)
        c1 = mapper['plot_obs_id'].isin(nh3_v.index)
        c2 = mapper['year'].isin(nh3_v.columns)
        mapper = mapper.loc[c1 & c2]
        self.eva.loc[c1 & c2, 'nh3_mg_m2_v'] = nh3_v.lookup(mapper['plot_obs_id'], mapper['year'])

        # NOx - Forest to all plots
        mapper = self.eva.assign(year=('year_' + self.eva.date_of_recording.astype(int).astype(str)),
                                 plot_obs_id=self.eva.index)
        c1 = mapper['plot_obs_id'].isin(nox_f.index)
        c2 = mapper['year'].isin(nox_f.columns)
        mapper = mapper.loc[c1 & c2]
        self.eva.loc[c1 & c2, 'nox_mg_m2_f'] = nox_f.lookup(mapper['plot_obs_id'], mapper['year'])

        # NOx - Open to all plots
        mapper = self.eva.assign(year=('year_' + self.eva.date_of_recording.astype(int).astype(str)),
                                 plot_obs_id=self.eva.index)
        c1 = mapper['plot_obs_id'].isin(nox_v.index)
        c2 = mapper['year'].isin(nox_v.columns)
        mapper = mapper.loc[c1 & c2]
        self.eva.loc[c1 & c2, 'nox_mg_m2_v'] = nox_v.lookup(mapper['plot_obs_id'], mapper['year'])

        # Drop NAs
        self.eva.dropna(axis=0, subset=['nh3_mg_m2_f', 'nh3_mg_m2_v', 'nox_mg_m2_f', 'nox_mg_m2_v'], how='any',
                        inplace=True)
        self.update_status()

        # Total NDep Forest plots
        self.eva['totN_mg_m2_f'] = self.eva.loc[:, ["nh3_mg_m2_f", "nox_mg_m2_f"]].sum(axis=1)
        self.eva['totN_kg_ha_f'] = self.eva.loc[:, 'totN_mg_m2_f'].divide(100)
        self.eva['totN_kmol_ha_f'] = self.eva.loc[:, 'totN_kg_ha_f'].divide(14)

        # Total NDep Open plots
        self.eva['totN_mg_m2_v'] = self.eva.loc[:, ["nh3_mg_m2_v", "nox_mg_m2_v"]].sum(axis=1)
        self.eva['totN_kg_ha_v'] = self.eva.loc[:, 'totN_mg_m2_v'].divide(100)
        self.eva['totN_kmol_ha_v'] = self.eva.loc[:, 'totN_kg_ha_v'].divide(14)

        # Total NDep differentiated according to plot Open/Forest classification
        if hasattr(self.eva, 'hooglaag'):
            f = self.eva.loc[self.eva.hooglaag == 'hoog'].index  # Forest
            v = self.eva.loc[self.eva.hooglaag == 'laag'].index  # Open vegetation
            x = self.eva.index.difference(f.union(v))  # Anders/niet bekend

            self.eva.loc[f, 'totN_kmol_ha_vf'] = self.eva.loc[f, 'totN_kmol_ha_f']
            self.eva.loc[v, 'totN_kmol_ha_vf'] = self.eva.loc[v, 'totN_kmol_ha_v']
            self.eva.loc[x, 'totN_kmol_ha_vf'] = self.eva.loc[x, 'totN_kmol_ha_v']  # Gebruik Open Veg waneer onduidelijk

        else:
            raise Exception('Cannot assign NDep data differenntiated to structuurtype. ')

        self.update_status(covar=['totN_kmol_ha_vf'])
        msg = '\nAdded NDep from POSCH with distinction open-forest: {0} rows remaining.\n\n'.format(self.eva.shape[0])
        self.report += msg
        if self.verbose:
            print(msg)

    def add_covar(self, covar_dir, covar_src, covar_name, nominal=False, keep_all=False, raster=True, **kwargs):
        """
        Sample values from a geospatial raster and add as new column
        :param covar_dir: directory containing the raster
        :param covar_src: *.tif geospatial raster with CRS assumed to be EPSG::3035
        :param covar_name: name as to apepear in the column
        :param nominal: boolean: nominal raster or not, default = False
        :param keep_all: boolean: join how = left if True else 'inner'
        :param raster: boolean, if True covar comes from a Raster datasource, if False from polygon. Def True
        :return: updated self.eva with column covar name
        """

        if not os.path.exists(os.path.join(covar_dir, covar_src)):
            raise OSError('Covariable does not exist: {0}'.format(os.path.join(covar_dir, covar_src)))

        if raster:
            # Get raster values based on 3035 easting-northing
            cov_df = do.get_raster_vals(coords=self.eva.plot_coordinates_3035,
                                        rast_src=os.path.join(covar_dir, covar_src), nominal=nominal)

            # translate raster values to categories if nominal
            if nominal:
                nominal_dict = do.get_raster_nominals(covar_name)
                cov_df['label'] = cov_df.vals.map(nominal_dict)

        else:
            # Get the values from a column in a polygon shapefile
            column = kwargs.get('column', Exception('Provide column keyword argument'))
            covar_gdf = gp.read_file(os.path.join(covar_dir, covar_src))
            if not all(covar_gdf.geometry.is_valid) or any(covar_gdf.geometry.isna()):
                # TODO: Identify invalid features
                warn = '  !! invalid geometries detected for {0}'.format(covar_src)
                raise Exception(warn)
            if any(covar_gdf.loc[:, column].isna()):
                warn = '  !! na values detected for column {}'.format(column)
                raise Exception(warn)
            try:
                joined = gp.sjoin(left_df=self.eva, right_df=covar_gdf, how='left', op='within', lsuffix='eva_',
                                  rsuffix='covar_')
            except ValueError:
                joined = gp.sjoin(left_df=self.eva.sample(self.eva.shape[0]), right_df=covar_gdf, how='left',
                                  op='within', lsuffix='eva_', rsuffix='covar_')
                # To catch ValueError: Null geometry supports no operations
            assert joined.shape[0] == self.eva.shape[0], '  spatial join shape does not match self.eva shape'
            cov_df = pd.DataFrame(data={'label': joined.loc[:, column].values}, index=joined.index)

        # drop NA, rename columns to reflect covariable name
        cov_df.dropna(axis=0, how='any', inplace=True)
        new_colnames = [covar_name + '_{0}'.format(x) for x in cov_df.columns.tolist()]
        cov_df.rename(columns=dict(zip(cov_df.columns.tolist(), new_colnames)), inplace=True)

        # join to self.eva
        self.eva = self.eva.join(other=cov_df, how='left' if keep_all else 'inner')

        self.update_status(covar=new_colnames)
        msg = 'Added Covariable {0} from {1} with {2} plots remaining.\n'.format(covar_name, covar_src,
                                                                                 self.status['n_plots'])
        if self.verbose:
            print(msg)
        self.report += msg

    def add_yearly_covar(self, covar_dir, covar_src_basename, covar_name, nominal=False):
        """
        Add a covariable column to self.eva that is differentiated per year based on yearly geospatial rasters
        :param covar_dir: directory containing the geospatial rasters
        :param covar_src_basename: basename of yearly rasters. Assumed: <covar_src_basename><year>.tif
        :param covar_name: name of the covariable as to appear in self.eva columns
        :param nominal: boolean, is the covariable nominal?
        :return: additional column covar_name to self.eva
        """

        # get all years present in the EVA header dataframe
        years = set(self.eva.date_of_recording.astype(np.uint16))

        # loop through years and add covariable for each. Make sure to retain all rows
        for year in years:
            src = os.path.join(covar_dir, '{0}{1}.tif'.format(covar_src_basename, year))
            sel_eva = self.eva.loc[self.eva.date_of_recording == year, :].index
            vals = do.get_raster_vals(coords=self.eva.loc[sel_eva, 'plot_point_wgs84'],
                                      rast_src=src, nominal=nominal)
            # Q&D solution to use plot_point_wgs84: I know the temperature and precipitation rasters are in WGS84!
            # Sorry not sorry
            self.eva.loc[sel_eva, covar_name] = vals.vals

        self.eva.dropna(axis=0, subset=[covar_name], how='any', inplace=True)
        self.update_status(covar=[covar_name])
        self.report += 'Added Covariable {0} from {1} with {2} plots remaining.\n'.format(covar_name, covar_dir,
                                                                                          self.status['n_plots'])

    def eunis2structuur(self):
        """
        Add doorvertaling van EUNIS type to self.eva, based on vertalingstabellen in Teams
        EUNIS --> Structuurtype
        EUNIS --> Open/Bos onderscheid
        Doorvertaling is onderscheidend naar oude/nieuwe EUNIS_code, gebaseerd op self.eva.eunis_src kolom.
        :return: self.eva.eunis_structuurtype
                 self.eva.openbos
        """

        # Read naming conventions and downstream classifcation of EUNIS types
        eunis_new_cats = pd.read_excel(r'c:\Users\roelo008\Wageningen University & Research\DOREN - General\DOREN-2020-12-02.xlsx',
                                       sheet_name='EUNIS_structuur')
        eunis_old_cats = pd.read_excel(r'c:\Users\roelo008\Wageningen University & Research\DOREN - General\DOREN-2020-12-02.xlsx',
                                       sheet_name='oude_EUNIS_structuur')
        eunis_new_cats.fillna(value='?', inplace=True)
        eunis_old_cats.fillna(value='?', inplace=True)

        # mappings from old/new eunis types to Wieger Wamelink categories. Also include "?" as key
        eunis_new2structuur = {**dict(zip(eunis_new_cats.eunis, eunis_new_cats.type)), **{'?': '?'}}
        eunis_new2bos = {**dict(zip(eunis_new_cats.eunis, eunis_new_cats.hoog_laag)), **{'?': '?'}}
        eunis_old2structuur = {**dict(zip(eunis_old_cats.EUNIS_OLD, eunis_old_cats.type)), **{'?': '?'}}
        eunis_old2bos = {**dict(zip(eunis_old_cats.EUNIS_OLD, eunis_old_cats.hoog_laag)), **{'?': '?'}}

        # Verify that all provided EUNIS codes are covered in one of the translation dictionaries.
        missing = set(self.eva.eunis_code).difference(set().union(*[set(eunis_new2structuur.keys()),
                                                                    set(eunis_new2bos.keys()),
                                                                    set(eunis_old2structuur.keys()),
                                                                    set(eunis_old2bos.keys())]))

        msg1 = 'Adding doorvertaling van EUNIS type naar structuurtype.\n'
        if len(missing) > 0:
            msg2 = 'EUNIS codes w/o doorvertaling naar vegtype:\n'
            msg3 = '\n'.join('{0}: {1}'.format(a, b) for a, b in enumerate(list(missing)))
            self.report += msg1 + msg2 + msg3 + '\n'
        else:
            self.report += msg1

        # Where to use old and new EUNIS?
        use_new_indx = self.eva.loc[self.eva.eunis_src == 'eunis_new'].index
        use_old_indx = self.eva.loc[self.eva.eunis_src == 'eunis_old'].index

        # Map structuurtype and OpenBos columns
        self.eva.loc[use_new_indx, 'structuurtype'] = self.eva.loc[use_new_indx, 'eunis_code'].map(eunis_new2structuur)
        self.eva.loc[use_old_indx, 'structuurtype'] = self.eva.loc[use_old_indx, 'eunis_code'].map(eunis_old2structuur)
        self.eva.loc[use_new_indx, 'hooglaag'] = self.eva.loc[use_new_indx, 'eunis_code'].map(eunis_new2bos)
        self.eva.loc[use_old_indx, 'hooglaag'] = self.eva.loc[use_old_indx, 'eunis_code'].map(eunis_old2bos)
        ''' 
        https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.map.html
        pd.series.map(dict) geeft geen KeyError als de Key niet in de dict zit, maar geeft NAN Dit is overduidelijk 
        het geval vooor eunis_new2structuur['A25c']. Dit EUNIS type heeft WW **niet** doorvertaald in de EUNIS_new
        Excel maar wordt **wel** genoemd als EUNIS_NEW. Dit resulteert in een NaN die daarna wordt opgevuld met "?"

        Update: A25c volgens de oude typologie is niet equivalent aan de nieuwe typologie. Ie code is hetzelfde, 
        inhoud niet

        '''

        self.eva.structuurtype.fillna('?', inplace=True)
        self.eva.hooglaag.fillna('?', inplace=True)

        # Append unique structuurtypes as a list
        self.structuurtypes = [x for x in set(self.eva.structuurtype) if x != '?']
        self.report += 'De structuurtypes zijn: \n'
        self.report += '\n'.join(self.structuurtypes)


    def select_plts_w_species(self, species_name):
        """
        Idenitfy all plots that contain a species
        :param species_name: the species
        :return: self.positive_plots: plot IDs of all plots containing species_name
                 self.negative_plots: plot IDs of all plots not containing species_name
        """
        if species_name in self.species:
            msg, proceed = '  succes: {0} present in EVA database\n'.format(species_name), True
        else:
            msg, proceed = '  fail: {0} absent in EVA database\n'.format(species_name), False

        if self.verbose:
            print(msg)
        self.report += msg

        if not proceed:
            raise OSError(msg)

        self.reset_species_sel()

        """header data of plots containing the species"""
        self.sel_species = species_name
        self.sel_species_ascii = self.sel_species.replace('ë', 'e').encode('ascii', 'replace').decode('UTF-8')
        self.positive_plots = set(self.spec.plot_obs_id[self.spec.species_name_hdr == species_name])
        self.negative_plots = self.eva.index.difference(self.positive_plots)

    def get_bedekking_selected_sp(self):
        """
        Create Series of bedekking of self.sel_species in all positive plots
        :return: pandas Series as new attribute self.sel_sp_coverage
        """
        extract = self.spec.loc[self.spec.species_name_hdr == self.sel_species, ['plot_obs_id', 'cover_perc']]
        # Pivot table to accomodate plots where self.sel_species occurs > 1. You can never know...
        # Also neatly sets the plot ID as index
        coverage = pd.pivot_table(data=extract, index='plot_obs_id', values='cover_perc', aggfunc='mean').dropna()
        self.sel_sp_coverage = pd.Series(data=coverage.cover_perc).astype(np.uint8)

        if coverage.shape[0] != len(self.positive_plots):
            # There are positive plots but without bedekking. Remove from positive plots
            self.positive_plots = coverage.index
            self.negative_plots = self.eva.index.difference(self.positive_plots)


    def filter_by_buffer_around_positive_plots(self, buffer_size):
        """
        Filter plots by buffering X meter around self.positive plots
        : param buffer_size: buffer size in meters.
        :returns self.buffer_gdf: geodataframe with polygon geometry of all buffers *buffer_size* around
                                  self.positive_plots
                 self.nearby_plots: plots w/o self.sel_species but within buffer
                 self.buffer_size: radius of the buffer
        """

        self.reset_buffering()

        if self.positive_plots == None:
            raise OSError('Cannot buffer because no positive plots are present')

        """create buffers around each selected veg plot"""
        buffers = self.eva.loc[self.positive_plots].buffer(buffer_size)
        buff = shapely.ops.unary_union(buffers)
        buff_gdf = gp.GeoDataFrame(data={'ID': 1, 'soort': self.sel_species}, geometry=[buff],
                                   index=pd.RangeIndex(len(buff) if hasattr(buff, 'len') else 1),
                                   crs={"init": "epsg:3035"})

        '''Get all plots without species but that are within the buffer'''
        nearby_plots = gp.sjoin(left_df=self.eva.loc[self.negative_plots], right_df=buff_gdf, op='within',
                                how='inner').index

        msg = '  found {0} plots with {1} and {2} plots without inside {3}m radius buffer\n'\
              .format(len(self.positive_plots), self.sel_species, len(nearby_plots), buffer_size)
        self.report += msg
        if self.verbose:
            print(msg)

        self.buffer_gdf = buff_gdf
        self.nearby_plots = nearby_plots
        self.buffer_size = buffer_size

    def nearest_positive(self):
        """
        Determine distance of each negative plots to nearest positive plot.
        :return: column 'dist2nearest' in self.eva with distance to nearest positive plot (0 for positive plots)
                 column 'nearestID' in self.eva with ID of nearest positive plot
        """

        # TODO: combine nearest_positive_of_type function into this one using **kwargs

        ids, dist = do.ckdnearest(gdA=self.eva.loc[self.negative_plots, :], gdB=self.eva.loc[self.positive_plots],
                                  id_col='plot_obs_id')

        self.eva.loc[self.negative_plots, 'dist2nearest_+'] = dist
        self.eva.loc[self.positive_plots, 'dist2nearest_+'] = 0
        self.eva.loc[self.negative_plots, 'nearest_id'] = ids
        self.eva.loc[self.positive_plots, 'nearest_id'] = self.eva.loc[self.positive_plots, 'plot_id']

        msg = '  for {0} negative plots, calculated distance to nearest plot with {1}. Min: {2}, ' \
              'max: {3}\n'.format(len(self.negative_plots), len(self.positive_plots), dist.min(), dist.max())
        self.report += msg
        if self.verbose:
            print(msg)

    def nearest_positive_queryd(self, col, val):
        """
        Determine distance of each negative plot to nearest postive plot restricted to postive plots meeting requirement
        self.eva.col == val
        :param col: column of self.eva to make selection on
        :param val: value of column to query
        :return: self.eva column 'dist2nearest<val>
                 self.eva. column 'nearest_<val>_id
        """

        assert hasattr(self, 'positive_plots'), 'Select plots with a species first'

        # Restrict gdB to postive plots AND query outcome
        query = self.positive_plots.intersection(self.eva.query('{0} == "{1}"'.format(col, val)).index)

        # Postive plots that fail to meet the query
        other = self.positive_plots.difference(query)

        if len(query) > 0:
            ids, dist = do.ckdnearest(gdA=self.eva.loc[self.negative_plots, :], gdB=self.eva.loc[query, :],
                                      id_col='plot_obs_id')
        else:
            ids = pd.Series([0]*len(self.negative_plots))
            dist = pd.Series([np.nan]*len(self.negative_plots))

        # Create distance to nearest column
        colname = val.replace(' ', '_')
        self.eva.loc[self.negative_plots, 'dist2nearest_+_{}'.format(colname)] = dist
        self.eva.loc[query, 'dist2nearest_+_{}'.format(colname)] = 0
        self.eva.loc[other, 'dist2nearest_+_{}'.format(colname)] = np.nan

        # Create nearest ID column
        self.eva.loc[:, 'nearest_+_{}_id'.format(colname)] = self.eva.plot_obs_id
        self.eva.loc[self.negative_plots, 'nearest_+_{}_id'.format(colname)] = ids.astype(self.eva.plot_obs_id.dtype)
        self.eva.loc[query, 'nearest_+_{}_id'.format(colname)] = self.eva.loc[query, 'plot_obs_id']
        self.eva.loc[other, 'nearest_+_{}_id'.format(colname)] = np.nan

    def overwrite_eunis(self, target_eunis, source_file, col):
        """
        Manually overwrite assigned EUNIS type of selected plot_ids to a new EUNIS type
        :param target_eunis: new EUNIs type
        :param source_file: source file containing plot IDs
        :param col: column in source_file
        :return: updated self.eva and self.report
        """

        df = pd.read_csv(source_file, sep='\t')
        plot_ids = df[col]
        sel_plot_ids = set(plot_ids).intersection(self.eva.index)

        self.eva.loc[sel_plot_ids, 'eunis_new'] = target_eunis
        msg = 'Manual overwrite EUNIS type for {0} plots to {1} based on {2}\n' \
            .format(len(sel_plot_ids), target_eunis, source_file)
        self.report += msg

    def write_stuff(self, what, covars=False):
        """
        Write contents to file. This should prob be organised as: self.generate_report(X) and then self.write_report(x)
        TODO: zet dit in een aparte klasse of functies
        """

        # preliminaries
        out_dir = os.path.join(self.base_out_dir, '{0}_{1}'.format(self.basename, self.timestamp))
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)

        if what == 'typische_soorten':
            '''Write requested species to file'''
            csv_out_dir = os.path.join(out_dir, 'csv')
            if not os.path.isdir(csv_out_dir):
                os.mkdir(csv_out_dir)
            req_out_name = '{0}_{1}_typische_soorten.csv'.format(self.basename, self.timestamp)

            self.request_df.to_csv(os.path.join(csv_out_dir, req_out_name), sep=',', index=False)

        elif what == 'species_list':
            '''write species list and frequencies to file'''

            csv_out_dir = os.path.join(out_dir, 'csv')
            if not os.path.isdir(csv_out_dir):
                os.mkdir(csv_out_dir)
            csv_out_name = '{0}_{1}_Species_Inventory.csv'.format(self.basename, self.timestamp)

            sp_inv = pd.pivot_table(data=self.spec, index='species_name_hdr', values='plot_obs_id', aggfunc='count')
            sp_inv['species_nr'] = sp_inv.index.map(self.species2nr).tolist()
            sp_inv.rename(columns={'plot_obs_id': 'count'}, inplace=True)
            sp_inv.sort_values(by='count', ascending=True).to_csv(os.path.join(csv_out_dir, csv_out_name),
                                                                  sep=';', index=True)

            self.report += 'Written Species list 2 file: {0}\n\n'.format(os.path.join(csv_out_dir, csv_out_name))

        elif what == 'report':
            '''Write processing report'''

            report_name = '{0}_{1}_processing_report.txt'.format(self.basename, self.timestamp)
            self.report += 'Written processing report to file: {0}\n\n'.format(os.path.join(out_dir, report_name))

            header = 'Processing report for {0} created {1}\n\n'.format(self.basename, self.timestamp)
            footer = '\nMade with Python 3.5 using Pandas by Hans Roelofsen, WEnR team B&B.'
            source = '\nSee git for source script: https://git.wur.nl/roelo008/doren_2019.'
            with open(os.path.join(out_dir, report_name), 'w') as f:
                f.write(header)
                f.write(self.report)
                f.write(footer)
                f.write(source)

        elif what == 'eva_headers':

            csv_out_dir = os.path.join(out_dir, 'csv')
            if not os.path.isdir(csv_out_dir):
                os.mkdir(csv_out_dir)

            headers_name = '{0}_{1}_EVA_headers.csv'.format(self.basename, self.timestamp)
            self.eva.drop(labels=['plot_coordinates_wgs84', 'plot_coordinates_3035', "plot_point_wgs84"], axis=1). \
                to_csv(os.path.join(out_dir, headers_name), sep=';')

            self.report += 'Written EVA headers to file: {0}\n\n'.format(os.path.join(csv_out_dir, headers_name))

        elif what == 'species_PG':
            '''
            Write txt file for use in FORTRAN programming as specified by Paul Goedhart:
            1: plotID - SpeciesID, sorted by plot ID, for self.positive_plots but drop all species other than
                 self.buffer_species
            '''

            if self.sel_species is None or 'totN_mol_ha' not in self.eva.columns:
                print('Cannot report for PG')
                return None

            # Dedicated directory for the PG output
            pg_dir = os.path.join(out_dir, 'pg_input')
            if not os.path.isdir(pg_dir):
                os.mkdir(pg_dir)

            pg_out_name = '{0}.txt'.format(self.sel_species.replace(' ', '_'))

            # Second, for self.positive_plots file with plot ID and Species Nr of self.buffer_species ONLY
            # Sort by plot ID
            # Filename = <self.buffer_species>.txt
            sp_sel = pd.DataFrame(data={'plot_id': list(self.positive_plots),
                                        'species_nr': self.species2nr[self.sel_species]}).sort_values(by='plot_id')
            sp_sel.to_csv(os.path.join(pg_dir, pg_out_name), sep=' ', index=False, header=False)

            self.report += 'Written FORTRAN input to files: {0}\n\n'.format(os.path.join(pg_dir, pg_out_name))

        elif what == 'NDep_PG':
            '''
            Write txt file for use in FORTRAN programming as specified by Paul Goedhart:
            1: plotID - NDep, sorted by plot ID for self.positive_plots AND self.nearby_plots, plus covars if Tru
            '''

            if self.sel_species is None or 'totN_mol_ha' not in self.eva.columns:
                print('Cannot report for PG')
                return None

            # Dedicated directory for the PG output
            pg_dir = os.path.join(out_dir, 'pg_input')
            if not os.path.isdir(pg_dir):
                os.mkdir(pg_dir)

            pg_out_name = '{0}_NDep_{1}'.format(self.sel_species.replace(' ', '_'), str(int(self.buffer_size / 1000)))

            # First file with plot headers and NDep plus covars if requested, sorted by plot header ID
            if covars:
                out_cols = ['totN_mol_ha'] + self.status['covars']
                pg_out_name += '_cv'
            else:
                out_cols = ['totN_mol_ha']
            self.eva.loc[self.positive_plots.union(self.nearby_plots), out_cols].sort_index(axis=0, ascending=True)\
                .to_csv(os.path.join(pg_dir, '{}.txt'.format(pg_out_name)), sep=',' if covars else ' ', index=True,
                        header=True if covars else False)

            self.report += 'Written FORTRAN input to files: {0}\n\n'.format(os.path.join(pg_dir, pg_out_name))

        elif what == 'species_single_file':
            """
            Single file for all plots with columns: plotID, dist2nearest, neareastID, respons. 
            Respons means 1/0 for self.positive_plots and self.negative_plots
            """

            # check if all required data is present
            if self.sel_species is None or not any([x.startswith('nearest') for x in list(self.eva)]):
                print('Cannot report for PG')
                return None

            # Dedicated directory for the PG output and output filename
            # TODO: replace non-ascii characters
            pg_out_name = '{0}_NDep_Nearest'.format(self.sel_species_ascii.replace(' ', '_'))
            pg_dir = os.path.join(out_dir, 'pg_input')
            if not os.path.isdir(pg_dir):
                os.mkdir(pg_dir)

            # Compile dataframe for writing to file
            nearest_cols = [col for col in list(self.eva) if col.startswith('dist2')]
            id_cols = [col for col in list(self.eva) if col.startswith('nearest')]

            out_df = self.eva.loc[:, ['plot_obs_id'] + nearest_cols + id_cols]
            out_df.loc[self.positive_plots, 'respons'] = np.uint8(1)
            out_df.loc[self.negative_plots, 'respons'] = np.uint8(0)
            as_type = {'respons': np.byte}

            # Join bedekking
            if hasattr(self, 'sel_sp_coverage'):
                out_df.loc[:, 'coverage'] = self.sel_sp_coverage
                out_df.coverage.fillna(np.uint8(0), inplace=True)
                as_type['coverage'] = np.uint8

            # Write file with plots obs ids, coverage, respons and dist to nearest each structuurtype and update report
            round_dict = dict.fromkeys(nearest_cols, 2)
            sel1 = out_df.loc[:, ['plot_obs_id', 'respons', 'coverage'] + nearest_cols]
            sel1.round(round_dict).astype(as_type) \
                .to_csv(os.path.join(out_dir, '{}.csv'.format(pg_out_name)), sep=',', index=False, header=True)
            self.report += 'Written single file output for species {0} to file: ' \
                           '{1}'.format(self.sel_species, os.path.join(pg_dir, pg_out_name))

            # Write to file w plot_obs_id and dist to nearest ID
            round_dict = dict.fromkeys(id_cols, 0)
            sel2 = out_df.loc[:, ['plot_obs_id', 'respons', ] + id_cols]
            sel2.round(round_dict).astype({'respons': np.byte}) \
                .to_csv(os.path.join(out_dir, '{}_ids.csv'.format(pg_out_name)), sep=',', index=False, header=True)

        elif what == 'plot_covars_file':
            """
            Single file with covariable information on all plots. 
            """

            # check if all required data is present
            if 'totN_kmol_ha' not in self.eva.columns:
                print('Cannot report plot covars file')
                return None

            # Dedicated directory for the PG output and output filename
            pg_out_name = 'Plot_Covars'
            pg_dir = os.path.join(out_dir, 'pg_input')
            if not os.path.isdir(pg_dir):
                os.mkdir(pg_dir)

            out_cols = ['plot_obs_id', 'totN_kmol_ha_v', 'totN_kmol_ha_f', 'totN_kmol_ha_vf', 'totN_kmol_ha',
                        'soil_type_label', 'country_label', 'five_yearly_precip', 'five_yearly_temp', 'eunis_code',
                        'eunis_src', 'structuurtype', 'hooglaag']
            for col in out_cols:
                assert hasattr(self.eva, col), 'Cannot write due to missing column {}'.format(col)
            out_df = self.eva.loc[:, out_cols]
            out_df.round({'plot_obs_id': 0, 'totN_kmol_ha': 2, 'five_yearly_precip': 2, 'five_yearly_temp': 2,
                          'totN_kmol_ha_v': 2, 'totN_kmol_ha_f': 2, 'totN_kmol_ha_vf': 2})\
                  .to_csv(os.path.join(out_dir, '{}.csv'.format(pg_out_name)), sep=',', index=False, header=True)
            self.report += 'Written plot covar output file: {0}'.format(os.path.join(pg_dir, pg_out_name))

        elif what == 'headers_shp':

            shp_out_dir = os.path.join(out_dir, 'shp')
            if not os.path.isdir(shp_out_dir):
                os.mkdir(shp_out_dir)
            shp_out_name = '{0}_{1}_EVA_headers.shp'.format(self.basename, self.timestamp)

            self.eva.drop(labels=['plot_coordinates_wgs84', 'plot_point_wgs84'], axis=1) \
                    .to_file(os.path.join(shp_out_dir, shp_out_name))

            self.report += 'Written EVA headers to Shapefile: {0}\n\n'.format(os.path.join(shp_out_dir, shp_out_name))

        elif what == 'buffer_png':
            '''Draw map of Europe with positive plots, buffers and nearby plots'''

            if self.buffer_gdf is None:
                print('Cannot draw map')
                return None

            png_out_dir = os.path.join(out_dir, 'buffer_png')
            if not os.path.isdir(png_out_dir):
                os.mkdir(png_out_dir)

            png_out_name = '{0}_{1}_{2}_{3}m.png'.format(self.basename, self.timestamp, self.sel_species,
                                                         self.buffer_size)

            # read additional shapefiles
            background = gp.read_file(self.background_shp)
            aoi = gp.read_file(self.aoi_shp)

            # set the canvas
            fig = plt.figure(figsize=(8, 10))
            ax = fig.add_subplot(111)
            ax.set(xlim=[2448000, 5427000], ylim=[1884000, 4888000])
            ax.set_aspect('equal')
            plt.tick_params(axis='both', labelbottom=False, labeltop=False, labelleft=False, labelright=False)

            # plot layers
            background.plot(ax=ax, facecolor='#729b6f', edgecolor="none")  # background countries
            aoi.plot(ax=ax, facecolor='#2d2c2c', alpha=0.3, edgecolor="none")  # AOI
            self.buffer_gdf.boundary.plot(ax=ax, edgecolor='#232323')  # buffers
            self.eva.loc[self.nearby_plots].plot(ax=ax, marker='o', color='#ff9e17', markersize=2, legend=True)
            self.eva.loc[self.positive_plots].plot(ax=ax, marker='o', color='#be2f00', markersize=2)
            background.boundary.plot(ax=ax, edgecolor='#365733')

            # legend
            legend_patches = [mpatches.Patch(label='Resident plots, n={:,}'.format(len(self.positive_plots)),
                                             facecolor='#be2f00'),
                              mpatches.Patch(label='Nearby plots, n={:,}'.format(len(self.nearby_plots)),
                                             facecolor='#ff9e17'),
                              mpatches.Patch(label='Buffers. Size={:,}m'.format(self.buffer_size), edgecolor='#232323',
                                             facecolor='none')]
            plt.legend(handles=legend_patches, loc='upper left', fontsize='small', frameon=True,
                       title=self.sel_species)

            # save to png
            fig.savefig(os.path.join(png_out_dir, png_out_name))
            plt.close()

            self.report += 'Written Buffer image to : {0}\n\n'.format(os.path.join(png_out_dir, png_out_name))

        else:
            print('{0} is not a valid reporting request'.format(what))

    def update_status(self, covar=None):
        """
        Update status of the object
        covar: name of covariable to be added to self.status['covars']
        :return: updates self.status
        """

        self.spec = self.spec.loc[self.spec.plot_obs_id.isin(self.eva.index)]  # remove from the species df
        self.species = set(self.spec.species_name_hdr)
        self.status.update(n_plots=self.eva.shape[0], columns=self.eva.columns.tolist(), n_species=len(self.species))
        if covar:
            if not isinstance(covar, list):
                covar_lst = [covar]
            else:
                covar_lst = covar
            self.status['covars'] += covar_lst

    def reset_species_sel(self):
        # reset self.positive_plots and self.negative_plots
        self.positive_plots = None
        self.negative_plots = None
        self.sel_species = None
        try:
            self.eva.drop(labels=['dist2nearest', 'nearest_id'], axis=1, inplace=True)
        except KeyError:
            pass

    def reset_buffering(self):
        # Reset all attributes related to buffering, to prevent plotting data related to species other than assumed
        self.buffer_gdf = None
        self.nearby_plots = None
        self.buffer_size = None

    def test(self):
        """
        Test if all contents of DOREN are as expected
        :return:
        """

        self.report += 'Testing dataset integrity\n'
        # Check for NAs
        for check_col in self.status['covars'] + ['plot_obs_id', 'date_of_recording', 'structuurtype', 'eunis_old',
                                                  'eunis_new', 'eunis_code', 'hooglaag', 'plot_coordinates_wgs84']:
            nas = self.eva.loc[:, check_col].isna()
            if any(nas):
                msg = '{0} -- {1} NA values found (index: {2})\n'.format(check_col, sum(nas),  self.eva.loc[nas].index)
            else:
                msg = '{0} -- no missing data\n'.format(check_col)
            self.report += msg

        # Check for invalid vals
        earliest_yr, latest_yr = self.eva.date_of_recording.min(), self.eva.date_of_recording.max()
        self.report += 'Plot years are between {0}-{1}\n'.format(earliest_yr, latest_yr)

        for check_col in [x for x in self.status['covars'] if not (x.endswith('precip') or
                                                                   self.eva.loc[:, x].dtype.name == 'object')]:
            sub_zeros = self.eva.loc[:, check_col] < 0
            if any(sub_zeros):
                msg = '{0} values below 0 found for {1} (index: {2})\n'.format(sum(sub_zeros), check_col,
                                                                               self.eva.loc[sub_zeros].index)
            else:
                msg = '{0} -- all values > 0\n'.format(check_col)
            self.report += msg

        if self.eva.index.difference(set(self.spec.plot_obs_id)).empty:
            msg = 'Full match between header- and species database plot IDs\n'
        else:
            eva_plots = self.eva.shape
            spec_plots = len(set(self.spec.plot_obs_id))
            msg = 'Difference found between EVA plot IDs {0} and plot IDs in species database {1}\n'.format(eva_plots,
                                                                                                            spec_plots)
        self.report += msg

        # Assert 1:1 doorvertaling EUNIS types naar structuurtypes
        piv = pd.pivot_table(data=self.eva, index='eunis_code', columns='structuurtype', values='plot_obs_id',
                             aggfunc='count')
        piv['stype_count'] = piv.notna().sum(axis=1)
        sel = piv.drop(piv.loc[piv.stype_count == 1].index)
        if sel.empty:
            self.report += 'Alle EUNIS typen vertalen 1:1 door naar een structuurtype.\n'
        else:
            self.report += 'Sommige EUNIS typen vertalen door naar > 1 structuurtype\n'
            self.report += sel.to_csv(sep='\t')

        # Check all NDep > 0
        ndeps = [col for col in dir(self.eva) if col.startswith('totN')]
        if all(self.eva.loc[:, ndeps] > 0):
            self.report += 'Alle NDep gegevens > 0\n'
        else:
            self.report += '!!! Sommige NDep gegevens <= 0\n'
