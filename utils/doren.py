"""
Helper functions for the DOREN project.

"""

import os
import json
import rasterstats as rast
import rasterio as rio
import geopandas as gp
import numpy as np
import re
import shapely
import pandas as pd
from shapely.geometry import Polygon
from scipy.spatial import cKDTree


# set pyproj project library if needed
if not os.environ['PROJ_LIB']:
    os.environ['PROJ_LIB'] = 'C:\\Users\\roelo008\\AppData\\Local\\ESRI\\conda\\envs\\hdr001\\Library\\share'
import pyproj


def eva_colnames_orig():
    """
    :return: list of columns names of the original EVA database
    """
    return ['PlotObservationID', 'PlotID', 'TV2 relevé number', 'Country', 'Cover abundance scale', 'Date of recording',
            'Relevé area (m²)', 'Altitude (m)', 'Aspect (°)', 'Slope (°)', 'Cover total (%)', 'Cover tree layer (%)',
            'Cover shrub layer (%)', 'Cover herb layer (%)', 'Cover moss layer (%)', 'Cover lichen layer (%)',
            'Cover algae layer (%)', 'Cover litter layer (%)', 'Cover open water (%)', 'Cover bare rock (%)',
            'Height (highest) trees (m)', 'Height lowest trees (m)', 'Height (highest) shrubs (m)',
            'Height lowest shrubs (m)', 'Aver. height (high) herbs (cm)', 'Aver. height lowest herbs (cm)',
            'Maximum height herbs (cm)', 'Maximum height cryptogams (mm)', 'Mosses identified (y/n)',
            'Lichens identified (y/n)', 'Remarks', 'Locality', 'Name association', 'Name alliance',
            'Full species list (y/n/?)', 'EUNIS', 'Longitude', 'Latitude', 'Location uncertainty (m)', 'Dataset',
            'EUNIS_Old', 'EUNIS_New']


def eva_colnames_new():
    """
    :return: list of improved colnames for the EVA database
    """
    return ['plot_obs_id', 'plot_id', 'tv2_releve_nr', 'country', 'cover_abundance_scale', 'date_of_recording',
              'releve_area_m2', 'altitude_m', 'aspect_deg', 'slope_deg', 'cover_total_perc', 'cover_tree_layer_perc',
              'cover_shrub_layer_perc', 'cover_herb_layer_perc', 'cover_moss_layer_perc', 'cover_lichen_layer_perc',
              'cover_algae_layer_perc', 'cover_litter_layer_perc', 'cover_open_water_perc', 'cover_bare_rock_perc',
              'height_highest_trees_m', 'height_lowest_trees_m', 'height_highest_shrubs_m', 'height_lowest_shrubs_m',
              'avg_height_high_herbs_cm', 'avg_height_lowest_herbs_cm', 'maximum_height_herbs_cm',
              'maximum_height_cryptogams_mm', 'mosses_identified_bool', 'lichens_identified_bool', 'remarks',
              'locality', 'name_association', 'name_alliance', 'full_species_list_bool', 'eunis', 'longitude',
              'latitude', 'location_uncertainty_m', 'dataset', 'eunis_old', 'eunis_new']


def get_aoi(*src):
    """
    Read an aoi shapefile from source, return as single ply after dissolving. If no arguments provided, return whole
    world
    :param src: path to the shapefile
    :return: geodataframe with one polygon
    """

    if src:
        aoi_raw = gp.read_file(src[0])
        aoi_diss = aoi_raw.dissolve(by='featurecla')
        return gp.GeoDataFrame(crs={"init": "epsg:3035"}, data={'aoi': 1}, index=[1], geometry=aoi_diss.geometry.values)
    else:
        return gp.GeoDataFrame(crs={"init": "epsg:3035"}, data={'aoi': 1}, index=[1],
                               geometry=[Polygon([(2555000, 5175000), (7098000, 5175000), (7098000, 1555000),
                                                  (2555000, 1555000)])])
                                                  #UL, UR, LR, LL
'''                                             
Extent in 3035
-3291518.0991032142192125,-6960799.0365917012095451 : 10028135.1418357975780964,6405773.0257125394418836
'''


def first_letter(x):
    """

    :param x: string
    :return: first letter of x
    """
    try:
        return x[0]
    except TypeError:
        return np.nan


def int_year(x):
    """
    Try to set x to integer, return nan if x is not a value
    :param x:
    :return:
    """
    try:
        out = np.int32(x)
    except ValueError:
        out = np.nan
    return out


def fix_eunis(eunis_code):
    """
    fix non standard eunis codes: doubles XX,YY and codes with an "!"
    "F31e,F53"   --> "F31e"
    "B14c!,E11a" --> "B14c"
    "B14c"       --> "B14c"
    :param eunis_code: EUNIS code
    :return: corrected EUNIS code as shown above
    """
    try:
        code = eunis_code.split(",")[0]
    except ValueError:
        code = eunis_code
    if code[0].isupper():
        return code.strip('!')
    else:
        return '?'


def eunis_to_veg_dict():
    # dictionary mapping EUNIS type to veg. type, see E-mail Wieger W 18-09-2019
    return {"A25a": "gras", "A25b": "gras", "A25c": "gras", "A25d": "gras", "B11a": "gras", "B11b": "gras",
            "B13a": "gras", "B13b": "gras", "B14a": "gras", "B14b": "gras", "B14b!,IE16": "gras", "B14c": "gras",
            "B14c!,E11a": "gras", "B14c!,E12b": "gras", "B15a": "heide", "B15b": "heide", "B15b,B16a": "heide",
            "B16a": "heide", "B21a": "gras", "B31a": "gras", "B31b": "gras", "B34a": "gras", "C11a": "gras",
            "C11b": "gras", "C12a": "gras", "C12b": "gras", "C14": "gras", "C15": "gras", "C16a": "gras",
            "C16b": "gras", "C1C2": "gras", "C21a": "gras", "C21b": "gras", "C22a": "gras", "C22b": "gras",
            "C23": "gras", "C24": "gras", "C25a": "gras", "C3": "gras", "C35a": "gras", "C35b": "gras",
            "C35c": "gras", "C35d": "gras", "C35e": "gras", "C51a": "gras", "C51b": "gras", "C52": "gras",
            "D": "gras", "D11": "gras", "D12": "gras", "D21": "gras", "D22a": "gras", "D22b": "gras",
            "D22c": "gras", "D23a": "gras", "D31": "gras", "D32": "gras", "D41a": "gras", "D41b": "gras",
            "D41c": "gras", "D42": "gras", "E": "gras", "E11a": "gras", "E11a!,E12b": "gras",
            "E11a!,E44b": "gras", "E11b": "gras", "E11d": "gras", "E11e": "gras", "E11f": "gras",
            "E11f,E12b": "gras", "E11g": "gras", "E11h": "gras", "E11i": "gras", "E11j": "gras", "E12a": "gras",
            "E12b": "gras", "E12b!,IE16": "gras", "E12c": "gras", "E13a": "gras", "E13b": "gras",
            "E13b!,E15b": "gras", "E13b!,F31c": "gras", "E13b!,F61a": "gras", "E13b!,F67": "gras",
            "E13b!,F74b": "gras", "E13c": "gras", "E15a": "gras", "E15a!,E15b": "gras", "E15a!,E18": "gras",
            "E15a!,E18!,E43b": "gras", "E15a!,E41!,F21,F22a": "gras", "E15a!,E43b": "gras",
            "E15a!,E43b!,F22a": "gras", "E15b": "gras", "E15b!,E18": "gras", "E15b!,F61a": "gras",
            "E15b!,F74a": "gras", "E15c": "gras", "E15c!,F74b": "gras", "E15c!,F74b,F74c": "gras", "E15d": "gras",
            "E15d!,E23": "gras", "E15d!,E44b": "gras", "E15d!,F74b,F74c": "gras", "E17": "gras", "E18": "gras",
            "E18!,E24": "gras", "E18!,F61a": "gras", "E19a": "gras", "E19b": "gras", "E1A": "gras", "E1B": "gras",
            "E21": "gras", "E22": "gras", "E23": "gras", "E23!,E43b": "gras", "E23!,E44b": "gras",
            "E23!,E52a": "gras", "E23!,F22a": "gras", "E23!,F22a,F42": "gras", "E23!,F42": "gras",
            "E23!,F74a": "gras", "E24": "gras", "E31a": "gras", "E32a": "gras", "E32b": "gras", "E33": "gras",
            "E34a": "gras", "E34b": "gras", "E35": "gras", "E41": "gras", "E41!,E43a": "gras",
            "E41!,E43a!,F11": "gras", "E41!,E43b": "gras", "E41!,F11": "gras", "E41!,F21": "gras",
            "E41!,F22a": "gras", "E43a": "gras", "E43a!,F11": "gras", "E43a!,F21": "gras", "E43b": "gras",
            "E43b!,E44b": "gras", "E43b!,F21": "gras", "E43b!,F21,F22a": "gras", "E43b!,F22a": "gras",
            "E43b!,F22a,F42": "gras", "E43b!,F42": "gras", "E43b!,F74a": "gras", "E44a": "gras", "E44b": "gras",
            "E44b!,F21": "gras", "E44b!,F22a": "gras", "E44b!,F22a,F42": "gras", "E44b!,F74b": "gras",
            "E44b!,F74b,F74c": "gras", "E52a": "gras", "E52a!,F42": "gras", "E52b": "gras", "E52b!,E53": "gras",
            "E53": "heide", "E53,F22a,F42": "heide", "E53,F42": "heide", "E53,F92": "heide", "E54": "gras",
            "E55": "gras", "E56": "gras", "E61": "gras", "E65": "gras", "F11": "heide", "F11,F12": "heide",
            "F11,F22a": "heide", "F11,F22b": "heide", "F11,F31a": "heide", "F11,F41": "heide",
            "F11,F41,F42": "heide", "F11,F42": "heide", "F11,F42,G3A": "heide", "F11,F42,G3Db": "heide",
            "F11,G3A": "heide", "F11,G3B": "heide", "F11,G3Db": "heide", "F12": "heide", "F21": "heide",
            "F21,F22a": "heide", "F21,F22a,F22b": "heide", "F22a": "heide", "F22a,F22b": "heide",
            "F22a,F23": "heide", "F22a,F24": "heide", "F22a,F31a": "heide", "F22a,F31b": "heide",
            "F22a,F31b,F74a": "heide", "F22a,F31c": "heide", "F22a,F31e": "heide", "F22a,F41": "heide",
            "F22a,F41,F42": "heide", "F22a,F42": "heide", "F22a,F42,F61a": "heide", "F22a,F42,F61a,F61b": "heide",
            "F22a,F42,F74a": "heide", "F22a,F51": "heide", "F22a,F53": "heide", "F22a,F61a": "heide",
            "F22a,F74a": "heide", "F22b": "heide", "F22b,F51": "heide", "F22b,F74a": "heide", "F22c": "heide",
            "F23": "heide", "F23,F31b": "heide", "F23,F31b,F92": "heide", "F23,F91": "heide", "F23,F92": "heide",
            "F24": "heide", "F31a": "heide", "F31a,F31c": "heide", "F31a,F31e": "heide", "F31a,F74b": "heide",
            "F31a,F74c": "heide", "F31b": "heide", "F31b,F31c": "heide", "F31b,F31e": "heide",
            "F31b,F31e,F31h": "heide", "F31b,F31g": "heide", "F31b,F74a": "heide", "F31b,F91": "heide",
            "F31b,F92": "heide", "F31b,G3A": "heide", "F31c": "heide", "F31c,F31e": "heide", "F31c,F51": "heide",
            "F31c,F74a": "heide", "F31d": "heide", "F31e": "heide", "F31e,F31f": "heide", "F31e,F31g": "heide",
            "F31e,F31g,F53": "heide", "F31e,F31h": "heide", "F31e,F53": "heide", "F31e,F54": "heide",
            "F31e,F61a": "heide", "F31e,F91": "heide", "F31e,F92": "heide", "F31e,F93": "heide",
            "F31e,G13": "heide", "F31f": "heide", "F31g": "heide", "F31g,F92": "heide", "F31h": "heide",
            "F31h,G11": "heide", "F31h,G12b": "heide", "F31h,G14": "heide", "F31h,G15": "heide",
            "F31h,G16a": "heide", "F31h,G16b": "heide", "F31h,G18": "heide", "F31h,G1Aa": "heide",
            "F31h,G1Ab": "heide", "F31h,G1C": "heide", "F31h,G31a": "heide", "F31h,G31b": "heide",
            "F31h,G34a": "heide", "F31h,G34c": "heide", "F31h,G3A": "heide", "F31h,G3B": "heide",
            "F31h,G3F2": "heide", "F41": "heide", "F41,F42": "heide", "F42": "heide", "F42,F61a": "heide",
            "F42,F61a,F61b": "heide", "F42,F61b": "heide", "F42,F74a": "heide", "F42,G3A": "heide",
            "F51": "struweel", "F51,F53": "struweel", "F51,F53,F61a": "struweel", "F51,F54": "struweel",
            "F51,F55": "struweel", "F51,F61a": "struweel", "F51,F61b": "struweel", "F51,F62": "struweel",
            "F51,F74a": "struweel", "F51,F93": "struweel", "F51,G22": "struweel", "F51,G24": "struweel",
            "F51,G37": "struweel", "F53": "struweel", "F53,F61a": "struweel", "F53,G24": "struweel", "F54": "struweel",
            "F54,G13": "struweel", "F54,G17a": "struweel", "F54,G1C": "struweel", "F54,G21": "struweel",
            "F55": "struweel", "F55,G24": "struweel", "F61a": "struweel", "F61a,F61b": "struweel",
            "F61a,F67": "struweel", "F61a,F74a": "struweel", "F61a,G24": "struweel", "F61b": "struweel",
            "F62": "struweel", "F62,F73": "struweel", "F62,G24": "struweel", "F62,G37": "struweel", "F67": "struweel",
            "F67,F68a": "struweel", "F68a": "struweel", "F68b": "struweel", "F68b,F73,F94": "struweel",
            "F68c": "struweel", "F71": "struweel", "F73": "struweel", "F73,F74c": "struweel", "F73,F94": "struweel",
            "F73,G24": "struweel", "F73,G37": "struweel", "F74a": "struweel", "F74b": "struweel",
            "F74b,F74c": "struweel", "F74c": "struweel", "F81": "struweel", "F91": "struweel", "F92": "struweel",
            "F92,G13": "struweel", "F93": "struweel", "F93,G13": "struweel", "F94": "struweel", "Fa": "struweel",
            "Fb": "struweel", "G": "bos", "G11": "bos", "G11,G12b": "bos", "G11,G14": "bos", "G12a": "bos",
            "G12a,G12b": "bos", "G12a,G13": "bos", "G12a,G22": "bos", "G12a,G3Db": "bos", "G12b": "bos",
            "G12b,G17a": "bos", "G12b,G18": "bos", "G12b,G26": "bos", "G13": "bos", "G13,G17a": "bos",
            "G13,G1Ba": "bos", "G13,G1C": "bos", "G13,G22": "bos", "G13,G25a": "bos", "G14": "bos", "G14,G15": "bos",
            "G14,G16b": "bos", "G14,G18": "bos", "G14,G19a": "bos", "G14,G1Ab": "bos", "G14,G1C": "bos",
            "G14,G3B": "bos", "G14,G3Da": "bos", "G14,G3Db": "bos", "G15": "bos", "G15,G19a": "bos", "G15,G19b": "bos",
            "G15,G3A": "bos", "G15,G3Da": "bos", "G15,G3Db": "bos", "G16a": "bos", "G16a,G17a": "bos",
            "G16a,G1Aa": "bos", "G16a,G1Ab": "bos", "G16b": "bos", "G17a": "bos", "G17a,G18": "bos", "G17a,G1Aa": "bos",
            "G17a,G22": "bos", "G17a,G24": "bos", "G17a,G37": "bos", "G17b": "bos", "G18": "bos", "G19a": "bos",
            "G19a!,G3A": "bos", "G19a,G3A": "bos", "G19a-,G3Db": "bos", "G19b": "bos", "G1Aa": "bos",
            "G1Aa,G1Ab": "bos", "G1Aa,G1C": "bos", "G1Aa,G31b": "bos", "G1Aa,G34a": "bos", "G1Aa,G37": "bos",
            "G1Aa,G3A": "bos", "G1Aa,G3B": "bos", "G1Aa,G3F2": "bos", "G1Ab": "bos", "G1Ab,G22": "bos",
            "G1Ba": "bos", "G1C": "bos", "G1C,G22": "bos", "G1C,I1": "bos", "G21": "bos", "G21!,G24": "bos",
            "G21!,G37": "bos", "G21,G22": "bos", "G21,G24": "bos", "G22": "bos", "G22,G37": "bos", "G24": "bos",
            "G24,G25a": "bos", "G24,G39b": "bos", "G25a": "bos", "G26": "bos", "G28": "bos", "G31a": "bos",
            "G31a,G3F2": "bos", "G31b": "bos", "G31c": "bos", "G32": "bos", "G32,G3Da": "bos", "G32,G3F2": "bos",
            "G34a": "bos", "G34b": "bos", "G34b,G34c": "bos", "G34b,G3B": "bos", "G34b,G3F2": "bos", "G34c": "bos",
            "G34c,G37": "bos", "G34c,G3F2": "bos", "G36": "bos", "G37": "bos", "G37,G39b": "bos", "G39a": "bos",
            "G39b": "bos", "G3A": "bos", "G3A,G3C": "bos", "G3B": "bos", "G3C": "bos", "G3Da": "bos",
            "G3Da,G3Db": "bos", "G3Da,G3F2": "bos", "G3Db": "bos", "G3F2": "bos", "H": "gras", "H21": "gras",
            "H22": "gras", "H23": "gras", "H24": "gras", "H25": "gras", "H26a": "gras", "H26b": "gras", "H26c": "gras",
            "H31a": "gras", "H31b": "gras", "H31c": "gras", "H31d": "gras", "H32a": "gras", "H32b": "gras",
            "H32c": "gras", "H32d": "gras", "H32f": "gras", "H34": "gras", "H51b": "gras", "I1": "gras",
            "I1,IE16": "gras", "I14": "gras", "IE16": "gras", "IE1E": "gras", "IE28": "gras", "IE51": "gras"}


def simplify_species(species_name):
    """
    Function to simplify plant species name to just family-species by removing subspecies etc
    Eg. Montia fontana subsp. fontana --> Montia fontana
    :param species_name:
    :return: species_name minus subsp or other flags
    """
    pattern = re.compile(r's.l.$| subsp.? | var.? | aggr.? | ""| ssp.? | s.? | mod.? | \(| aggr\.?$| sensu\.? ')
    if re.search(pattern, species_name):
        out = re.split(pattern, species_name)[0]
    else:
        out = species_name
    return out.strip()


def verify_species(species_name):
    """
    Verify if species name exists of "<Family> <species>" format
    :param species_name: species name
    :return: Boolean
    """
    pattern = re.compile(r'^[A-Z][a-z]+ [a-z]+$')
    if re.match(pattern, species_name):
        return True
    else:
        return False


def strip_leading_quote(species_name):
    """"
    There are some species names starting with a quote. Remove
    """
    pattern = re.compile('^"')
    if re.search(pattern, species_name):
        return re.split(pattern, species_name)[1]
    else:
        return species_name


def lon_lat_2_east_north(lon_lat):
    """
    Projects WGS84 latitude, longitude to EPSG:3035 LAEA Easting - Northing
    :param lon_lat: longitude, latitude in decimal degree tuple
    :return: easting, northing in m as a shapely Point geometry
    """

    lon, lat = lon_lat

    # parameters for projecting lat long to EPSG3035 easting northing
    in_proj = pyproj.Proj(init='epsg:4326')  # WGS84 geographic CRS
    out_proj = pyproj.Proj(init='epsg:3035')  # ETRS89-extended / LAEA Europe

    (east, north) = pyproj.transform(in_proj, out_proj, lon, lat)

    return shapely.geometry.Point((east, north))


def generate_square_id(east_north, size):
    """
    Used to slot a EVA plot into a geographical square based on its coordinates.

    :param east_north: tuple easting, northing coordinates in EPSG3035
    :param size: length in m of the squares (eg 10.000 or 25.000 m)
    :return: square identification string as eeee_nnnn: E-N of square lower left coordinates

        |
        |
      N |        X
        |
        @_____________
              E
        <------------>
             size

     Given plot at location X and square size x size, return Easting Northing of @ as "eeee_nnnn"
"""
    easting, northing = east_north

    sq_ll_e = easting // size  # easting of square lower left
    sq_ll_n = northing // size  # northing of square lower left

    return '{:04d}_{:04d}'.format(int(sq_ll_e), int(sq_ll_n))


def generate_square_shapes(sq_ids, size):
    """
    Generate polygon shapefie containing squares
    :param sq_ids: list of strings carrying lowerleft corner of each square
    :param size: size of the square in m
    :return: pandas geodataframe
    """

    # reduce to unique sq ids
    u_sq_ids = set(sq_ids)

    # reconstruct the easting - northing from the square IDs
    eastings = [np.multiply(np.int32(e), size) for e, _ in [ids.split('_') for ids in u_sq_ids]]
    northings = [np.multiply(np.int32(n), size) for _, n in [ids.split('_') for ids in u_sq_ids]]

    corners = [[(xmin, ymin), (xmin+size, ymin), (xmin+size, ymin+size), (xmin, ymin+size)] for (xmin, ymin) in
               list(zip(eastings, northings))]
    shapes = [shapely.geometry.Polygon(corner) for corner in corners]

    return gp.GeoDataFrame(crs={"init": 'epsg:3035'}, geometry=shapes, data={'ID': list(u_sq_ids), 'size': str(size)})


def get_raster_nominals(cov_name):
    """
    Get the nominal categories associated with a covariable raster
    :param cov_name: name of the covariable
    :return: dictionary of the raster values and corresponding nominals
    """

    if os.path.basename(os.getcwd()) == 'doren_2019':
        src = r'./utils/legend_mappings.json'
    elif os.path.basename(os.getcwd()) == 'a_prep':
        src = r'../utils/legend_mappings.json'
    else:
        src = r'c:/apps/proj_code/doren_2019/utils'  # fuck it

    with open(src, 'r') as j:
        all_mappings = json.load(j)

    try:
        mapping = all_mappings[cov_name]
        out = {}
        for k, v in mapping.items():
            out[int(k)] = np.nan if v == 'weg' else v
        return out
    except KeyError:
        print('No legend mapping available for {0}'.format(cov_name))
        raise


def get_raster_vals(coords, rast_src, nominal=False):
    """
    Query a raster for its values at each coordinate pair. Assumes CRS match between the coordinates series and the r
    raster image.
    :param coords: pandas series with shapely point geometries
    :param rast_src: source of raster image to be queried
    :param nominal: boolean, raster source is categorical (nomical) or not. Default = False
    :return: pandas dataframe w raster values and nominal category
    """

    if not os.path.isfile(rast_src):
        raise Exception('{0} does not exists as valid rastersource'.format(rast_src))
    raster = rio.open(rast_src)

    interpolation = 'nearest' if nominal else 'bilinear'

    if isinstance(coords.iloc[0], str):
        raise TypeError('Trying to parse string to coorindate tuples. Did you read EVA data from *.csv instead of pkl')

    if not isinstance(coords.iloc[0], shapely.geometry.point.Point) and isinstance(coords.iloc[0], tuple):
        print('converting coordinate tuples to Shapely points')
        coords = coords.apply(shapely.geometry.Point)

    # the numeric raster values
    rast_vals = rast.point_query(coords, raster.read(1), interpolate=interpolation, affine=raster.affine,
                                 nodata=raster.nodata)

    return pd.DataFrame(data={'vals': rast_vals}, index=coords.index)


def ckdnearest(gdA, gdB, id_col):
    """
    Nearest neighbour search using Scipy cKDTree.
    Courtesy: https://gis.stackexchange.com/questions/222315/geopandas-find-nearest-point-in-other-dataframe
    :param gdA: geodataframe containing origin points, ie negative plots
    :param gdB: geodataframe containing destination points, ie positive plots
    :return: series with gdA.index with nearest ID from gdB,
             series with gdA.index with distance to nearest gdB
    """

    nA = np.array(list(gdA.geometry.apply(lambda x: (x.x, x.y))))
    nB = np.array(list(gdB.geometry.apply(lambda x: (x.x, x.y))))
    btree = cKDTree(nB)
    dist, idx = btree.query(nA, k=1)

    nearest_ids = pd.Series(data=gdB.iloc[idx, gdB.columns.tolist().index(id_col)].values, index=gdA.index,
                            name='nearestID')
    dist2nearest = pd.Series(np.divide(dist, 1000), index=gdA.index, name='dist2nearest')

    return nearest_ids, dist2nearest

