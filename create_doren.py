
import os
import pickle
from utils import doren_classes as dc
# os.environ['GDAL_DATA'] = r'C:\Users\roelo008\Miniconda3\envs\doren\Library\share\gdal'

os.environ['PROJ_LIB'] = r'c:\Users\roelo008\Miniconda3\envs\doren\\Library\\share\\proj'
os.environ['GDAL_DATA'] = r'c:\Users\roelo008\Miniconda3\envs\doren\\Library\\share'

param_header_src = r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20201118\EVA_Doren_header.csv'
param_sp_src = r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_201909\EVA_Doren_species.csv'
param_aoi_src = r'W:\PROJECTS\Doren19\a_brondata\AOI\ne_50m_cntrs_AOI_diss_fin.shp'
param_dem_src = r'W:\PROJECTS\Doren19\a_brondata\covariables\DEM\DTM_3035.tif'
param_posch_single = r'W:\PROJECTS\Doren19\a_brondata\POSCH_dep\20200401delivery\v2'
param_posch_diff = r'W:\PROJECTS\Doren19\a_brondata\POSCH_dep\20201012delivery'
cv_soil_dir = r'w:\PROJECTS\Doren19\a_brondata\covariables\soil\b_processed'
cv_soil_src = 'WRBLEV1_laea.tif'
cv_cntr_dir = r'w:\PROJECTS\Doren19\a_brondata\covariables\countries'
cv_cntr_src = 'ne_50m_cntrs_sel_buff_diss_2_3035.shp'
cv_precp_dir = r'w:\PROJECTS\Doren19\a_brondata\covariables\EObs\2_compiled'
cv_precp_src = "EObs_v200e_rr_5yrmean"
cv_temp_dir = cv_precp_dir
cv_temp_src = "EObs_v200e_tg_5yrmean"
sp_req_src = r'c:\Users\roelo008\Wageningen University & Research\DOREN - General\DOREN-2020-12-02.xlsx'
sp_req_sheet, column, skip = 'Soorten', 'wetenschappelijke naam', 1
# sp_req_src = r'c:\Users\roelo008\Wageningen University & Research\DOREN - General\2020-09-17 uniek soorten per habitat (2).xlsx'

testing = False

doren = dc.Doren(header_src=param_header_src, sp_src=param_sp_src)
doren.initiate(sample=testing)
doren.apply_requirements('req1', 'req2', 'req3', 'req4', 'req8', 'req9', 'req10',
                         aoi_src=None if testing else param_aoi_src, dem_src=param_dem_src)
doren.overwrite_eunis(source_file=r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Calluna_Avenula.csv',
                      col='PlotObservationID', target_eunis='S42')
doren.overwrite_eunis(source_file=r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Calluna_Molinea.csv',
                      col='PlotObservationID', target_eunis='S42')
doren.overwrite_eunis(source_file=r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Empetrum_Avenula.csv',
                      col='PlotObservationID', target_eunis='S42')
doren.overwrite_eunis(source_file=r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Empetrum_Molinea.csv',
                      col='PlotObservationID', target_eunis='S42')
doren.overwrite_eunis(source_file=r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Erica_Avenula.csv',
                      col='PlotObservationID', target_eunis='S41')
doren.overwrite_eunis(source_file=r'W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Erica_Molinea.csv',
                      col='PlotObservationID', target_eunis='S41')
doren.merge_eunis()

doren.get_requested_species(xls=sp_req_src, sheet=sp_req_sheet, col=column, skip=skip, simplify_names=True)
doren.add_covar(covar_dir=cv_cntr_dir, covar_src=cv_cntr_src, covar_name='country', raster=False, column='SOV_A3')
doren.add_covar(covar_dir=cv_soil_dir, covar_src=cv_soil_src, covar_name='soil_type', nominal=True)
doren.add_yearly_covar(covar_dir=cv_precp_dir, covar_src_basename=cv_precp_src, covar_name='five_yearly_precip')
doren.add_yearly_covar(covar_dir=cv_temp_dir, covar_src_basename=cv_temp_src, covar_name='five_yearly_temp')

doren.eunis2structuur()
doren.add_posch_single(posch_src_dir=param_posch_single)
doren.add_posch_differentiated(posch_src_dir=param_posch_diff)
doren.test()


doren.write_stuff('plot_covars_file')
doren.write_stuff('report')
if not testing:
    with open(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\a_pkl\doren_{}.pkl'.
                      format(doren.timestamp), 'wb') as handle:
        pickle.dump(doren, handle, protocol=pickle.HIGHEST_PROTOCOL)

