"""
Script for taking a pre-existing DOREN pkl object and processing a subset of its requested species
Hans Roelofsen, 03 June 2020
"""
import numpy as np
import argparse
import pickle
import os

parser = argparse.ArgumentParser()
parser.add_argument('batch_nr', help='batch number')
parser.add_argument('n_batches', help='number of batches')
parser.add_argument('pkl', help='name of pkl source')
args = parser.parse_args()

batch_nr = int(args.batch_nr)
n_batches = int(args.n_batches)

# Recover the Pickled doren object
pkl_src = os.path.join('./pkl_src/', args.pkl)
# pkl_src = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\a_pkl\doren_20210113.pkl'
with open(pkl_src, 'rb') as handle:
    doren = pickle.load(handle)

doren.base_out_dir = r'./c_out'

# doren.req_sp = None
# sp_req_src = r'c:\Users\roelo008\Wageningen University & Research\DOREN - General\2020-09-17 uniek soorten per habitat (2).xlsx'
# doren.get_requested_species(xls=sp_req_src, sheet='Verdringingssoorten', col='wetenschappelijke naam', simplify_names=False)

if batch_nr == 1:
    # Write the covar file once
    doren.write_stuff('plot_covars_file')

# Determine subset of requested species to proces
sel = np.array_split(np.arange(0, len(doren.req_sp)), n_batches)[batch_nr].tolist()
print('Commencing processing of {0} species...'.format(len(sel)))

# Process subset of requested species
for i, j in enumerate(sel, start=1):
    sp = doren.req_sp[j]
    try:
        print('Doing species {0} of {1}: {2}'.format(i, len(sel), sp))
        doren.select_plts_w_species(species_name=sp)
        for structuurtype in doren.structuurtypes:
            doren.nearest_positive_queryd(col='structuurtype', val=structuurtype)
        doren.get_bedekking_selected_sp()
        doren.write_stuff('species_single_file')
    except OSError:
        continue

doren.write_stuff('report')
