# Dosis Effect Relaties Natuur (DOREN)

Dit repository bevat python code voor selectie van Vegetatieopnames uit de EVA database en ruimtelijke-koppeling met diverse GIS bestanden. Deze code is onderdeel van het DOREN project van Wageningen Environenmental Research (WEnR). Deze repository is onderdeel van Wamelink *et al* (2021) *Responsecurven van habitattypen voor stikstofdepositie*. 

Code door: Hans Roelofsen, Wageningen Environmental Research

Projectleider: Wieger Wamelink, Wageningen Environmental Research

Datum: september 2019 - januari 2021

### Brongegevens
  * Export van de EVA vegetetieopname database uit, bestaande uit: 
    * kopgegevens (headers) met opnameID, datum, latitude, longitude en andere metadata per plot
    * inventarisgegevens met soortnaam, soortnummer en bedekking voor alle soorten in elke opname
  * Shapefile met landsgrenzen (met wijdere begrenzing op zee om opnames langs de kustlijn mee te kunnen nemen). 
  * Geospatial raster met bodemtypes
  * Geospatial rasters met gemiddelde dagelijkse neerslagsom per jaar
  * *idem* voor jaarlijkse gemiddelde temperatuur
  * *\*.csv* files met N-depositie gegevens per opname (rijen) per jaar (kolommen) 
  * Excel file met daarin soortnamen waarvoor output gemaakt moet worden
  * Excel file met doorvertaling van EUNIS *vegetatietypen* naar *structuurtypen* en *ruwheidsfactorcategorieen*.

### Methode
De DOREN class is gedefinieerd als klasse om 
1. vegetatieopnames te lezen vanuit een file-bestand en intern op te slaan als [Pandas](https://pandas.pydata.org/) dataframe.
2. selectiecriteria toe te passen op de vegetatieopnames, bijvoorbeeld op jaartal.
3. aanvullende gegevens toe te voegen aan de opnames via GIS spatial joins
4. aanvullende gegevens toe te voegen aan de opnames op basis van de plotIDs en jaartallen
5. diverse tabellen en rapportages te genereren   
6. een Excel file inlezen met een lijst van plantensoorten waarvoor output gemaakt moet worden (*requested species*)
7. voor een plantensoort een tabel te genereren waarin: 
   1. per opname is aangegeven of de soort daarin aanwezig is ('positieve' opname)
   2. de bedekking van de soort in elke positieve opname
   3. de afstand van elke negatieve opname tot dichstbijzijnde positieve opname
    4. de afstand van elke negatieve opname tot dichstbijzijnde positieve opname *van een bepaalde structuurtype*
    
In `create_doren.py` worden de gewenste selecties op de EVA opnames toegepast en de resterende opnamen gekoppeld aan landsnaam, temperatuur, neerslag, N-depositie. Tevens worden de EUNIS vegetatietypes van elke opname doorvertaald naar een *structuurtype* en een *ruwheidsfactorcategorie*. Het DOREN class object genereert een rapportage waarin alle stappen worden vastgelegd en wordt daarna opgeslagen als [`pickle`](https://docs.python.org/3/library/pickle.html) object. 

De `pickle` wordt vervolgens gekopieerd naar de WUR [Anunna High Performance Computer](https://wiki.anunna.wur.nl/index.php/Main_Page). Hier wordt voor elke gewenste soort een soort-file gegenereerd middels het `run_species.py` script. Dit gebeurt gedistribueerd over meerdere HPC nodes, zodat de totale doorlooptijd beperkt blijft. Dit process wordt aangestuurd via `doren_batch.sh`.

### Benodigdheden
 * [python 3.5.6](https://www.python.org/)  
 * [geopandas 0.6.3](https://geopandas.org/install.html)
 * [numpy 1.15.2](https://numpy.org/)
 * [pandas 0.23.34](https://pandas.pydata.org/)
 * [rasterio 0.36.0](https://rasterio.readthedocs.io/en/latest/)
 * [rasterstats 0.14.0](https://pythonhosted.org/rasterstats/)
 * [xlrd 1.2.0](https://xlrd.readthedocs.io/en/latest/) 

### Meer informatie
Neem contact op met [Hans Roelofsen](https://www.wur.nl/en/Persons/Hans-dr.-HD-Hans-Roelofsen.htm).

### Copyright
Copyright 2021 Wageningen Environmental Research (instituut binnen de rechtspersoon Stichting Wageningen Research), Postbus 47, 6700 AA Wageningen, T 0317 48 07 00, [Wageningen Environmental Research](www.wur.nl/environmental-research). Wageningen Environmental Research is onderdeel van Wageningen University & Research.

* Overname, verveelvoudiging of openbaarmaking van deze uitgave is toegestaan mits met duidelijke bronvermelding.
* Overname, verveelvoudiging of openbaarmaking is niet toegestaan voor commerciële doeleinden en/of geldelijk gewin.
* Overname, verveelvoudiging of openbaarmaking is niet toegestaan voor die gedeelten van deze uitgave waarvan duidelijk is dat de auteursrechten liggen bij derden en/of zijn voorbehouden.