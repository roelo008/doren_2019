"""
Script for taking E-Obs temperature and precipitation NetCDF files. See: https://surfobs.climate.copernicus.eu/dataaccess/access_eobs.php#datafiles

Calculate 5 year averages for temp and precipitation for [yearX, X-1, X-2, X-3, X-4] where X is each year in the EObs
Dataset. Write 5 year averages to geospatial TIFs

Hans Roelofsen, WEnR, 21 april 2020
"""

import os
import numpy as np
import rasterio as rio
import pandas as pd
from netCDF4 import Dataset
import datetime

out_dir = r'c:\apps\temp_geodata\doren\temp_yearly'

temp_dir = r'c:\apps\temp_geodata\doren'
temp_in = "tg_ens_mean_0.1deg_reg_v20.0e.nc"
prec_dir = r'c:\apps\temp_geodata\doren'
prec_in = 'rr_ens_mean_0.1deg_reg_v20.0e.nc'

temp = Dataset(os.path.join(temp_dir, temp_in), 'r', format='NETCDF4')
prec = Dataset(os.path.join(prec_dir, prec_in), 'r', format='NETCDF4')

'''Identify all bands in the NetCDF as days of the year'''
a = datetime.date(1950, 1, 1)
z = datetime.date(2019, 7, 31)
delta = z-a
dates = [a + datetime.timedelta(days=i) for i in range(delta.days + 1)]

# dataframe with all bandas of the NetCDF and their corresponding dates
df = pd.DataFrame(data={'count': [i for i, _ in enumerate(dates)], 'date_str': [str(j) for j in dates],
                        'year': [d.year for d in dates], 'date': dates})

# find start and end index of the NetCDF file to identify 5 yearly intervals
df2 = pd.DataFrame(data={'year': list(set([x.year for x in dates]))})
df2['start'] = df2.apply(lambda row: min(df.loc[df['year'].isin([y for y in range(row.year-4, row.year+1)]), 'count']),
                         axis=1)
df2['end'] = df2.apply(lambda row: max(df.loc[df['year'].isin([y for y in range(row.year-4, row.year+1)]), 'count']),
                       axis=1)

for row_row in df2.itertuples():
    year = row_row.year
    start_day = row_row.start
    end_day = row_row.end

    print('Now doing year {0} with indexes {1} to {2}'.format(year, start_day, end_day))

    # get the corresponding arrays
    daily_temps = temp.variables['tg'][start_day:end_day, :, :]  # selects starting from, up to and including
    daily_prec = prec.variables['rr'][start_day:end_day, :, :]
    # note NetCDF dimensionality: [day_since_1-1-1950, row, col]

    # calc mean
    temp_5yr_mean = np.flipud(daily_temps.mean(axis=0))  # reduce to shape (465, 705)
    prec_5yr_mean = np.flipud(daily_prec.mean(axis=0))

    # also flip horizontally, because row,col (0,0) in the NetCDF array tg[0,:,;] corresponds to LOWER LEFT
    # in the geographical domain, not TOP LEFT as is usaul.

    # set sensible nodata values
    temp_5yr_mean[temp_5yr_mean.mask] = -99  # dit kan vast handiger, maar goed.
    prec_5yr_mean[prec_5yr_mean.mask] = -99  # sorry...

    # write to files
    basename = 'EObs_v200e_{0}_5yrmean{1}.tif'
    prof = {}
    prof.update(count=1, compress='LZW', dtype=rio.float32, driver='GTiff', width=temp.variables['longitude'].shape[0],
                height=temp.variables['latitude'].shape[0], nodata=-99, transform=(-25, 0.1, 0, 71.5, 0, -0.1),
                crs='EPSG:4326')

    with rio.open(os.path.join(out_dir, basename.format('tg', year)), 'w', **prof) as f:
        f.write(temp_5yr_mean.astype(np.float32), 1)

    with rio.open(os.path.join(out_dir, basename.format('rr', year)), 'w', **prof) as f:
        f.write(prec_5yr_mean.astype(np.float32), 1)

