'''
Script to verify delivery of EUNIS vegetation types by Stephan H, november 2020

'''

import pandas as pd
import pickle

eva_headers = r'W:\PROJECTS\doren19\a_brondata\EVA\EVA_Doren_header.csv'
eva_eunis = r'\\wur\dfs-root\PROJECTS\doren19\a_brondata\covariables\EUNIS\EVA-EUNIS.csv'
pkl_src = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\DOREN\b_compiled_data\a_pkl\doren_20201028.pkl'


with open(pkl_src, 'rb') as handle:
    doren = pickle.load(handle)

headers = pd.read_csv(eva_headers, sep='\t', skiprows=[0,1], index_col='PlotObservationID')
eunis = pd.read_csv(eva_eunis, sep='\t', index_col='PlotObservationID')
colnames_n = ['plot_id', 'releve_nr', 'date_of_recording', 'expert_system',
              'longitude', 'latitude', 'location_uncertainty_m', 'dataset', 'regime']
eunis.rename(columns=dict(zip(list(eunis), colnames_n)), inplace=True)

n_headers = headers.shape[0]
n_eunis = eunis.shape[0]

print('EVA headers: {}'.format(n_headers))
print('EUNIS headers: {}'.format(n_eunis))

t1 = "all EVA headers are present in the EUNIS database"

diff = headers.index.difference(eunis.index)
if diff.empty:
    print('{}: passed'.format(t1))
else:
    print('{}: failed'.format(t1))
    print('There are {0} EVA headers that are not present in the EUNIS db'.format(len(diff)))
    headers.loc[diff].to_clipboard(sep=';')


